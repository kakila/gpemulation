#include <iostream>
#include <Eigen/Dense>
#include <vector>
#include <cmath>
#include <cfloat>
#include <chrono>
#include <ctime>
#include <sstream>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::RowVectorXd;
using std::vector;
using std::exp;
using std::expm1;

void block_1d_real (const VectorXd & t, const RowVectorXd & r,
                 double const & a, double const & b,
                 MatrixXd & m);

//double EPS = std::numeric_limits<double>::epsilon ()

int main(int argc, char *argv[])
{
  int n = 3;
  size_t N;

  std::istringstream ss(argv[1]);
  if (!(ss >> N))
    std::cerr << "Invalid number " << argv[1] << std::endl;

  double dt = 1.0 / (N - 1);

  VectorXd t(N);
  RowVectorXd r(N);
  t(0) = 0;
  r(0) = 0;
  for ( size_t i=1; i < N; ++i)
  {
    t(i) = t(i-1) + dt;
    r(i) = t(i);
  }

  double a = -3, b = -1;

  MatrixXd G(N,N);

  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();

  block_1d_real (t,r,a,b,G);

  end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);

  std::cout << "finished computation at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n";
  //std::cout << G << std::endl;

  return 0;
}

void block_1d_real (const VectorXd & t, const RowVectorXd & r,
                 double const & v, double const & w,
                 MatrixXd & m)
{

  size_t nRows  = m.rows(), nCols = m.cols();
  double g0     = 0;
  double vw     = v + w;
  bool opposite = std::abs(vw) < DBL_EPSILON;

  for (size_t j = 0; j < nCols; ++j)
    for (size_t i = 0; i < nRows; ++i)
    {
      if (opposite)
      {
        g0 = exp (v * ( t(i) - r(j) ));
        if ( t(i) < r(j) )
          m(i,j) = g0 * t(i);
        else
          m(i,j) = g0 * r(j);
      }
      else
      {
        if ( t(i) < r(j) )
          m(i,j) = expm1 (vw * t(i)) * exp(w * (r(j) - t(i)));
        else
          m(i,j) = expm1 (vw * r(j)) * exp(v * (t(i) - r(j)));
      }
    }

  if (!opposite)
    m /= vw;
}
