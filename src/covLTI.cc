#include <octave/oct.h>
// PKG_ADD: autoload ("__greenLTIo1diag__", "covLTI.oct");
DEFUN_DLD (__greenLTIo1diag__, args, nargout,
           "Diagonal part (time dependence) of the covariance function of a LTI system.")
{
  octave_value_list retval;
  int nargin = args.length ();

  if (nargin < 1 || nargin > 3)
    print_usage ();
  else
    {

      RowVector d    = args(0).row_vector_value ();
      ColumnVector t = args(1).column_vector_value ();
      RowVector r    = args(2).row_vector_value ();

      Matrix(t) - Matrix(r);

      octave_idx_type nt = t.dims()(0);
      octave_idx_type nr = r.dims()(1);
      octave_idx_type m  = d.dims()(1);

      Matrix G(dim_vector(m*nt, m*nr), lo_ieee_na_value());

      // We loop first (inner loop) over the nt, nr time values
      // Then over the elements of the diagonal.
      octave_idx_type row_id, row, col_id, col;

      double eps = std::numeric_limits<double>::epsilon ();

      // diagonal elements looping //
      for (octave_idx_type l = 0; l < m; l++)
      {
        col_id = l * nr;      // column of covaraince matrix
        for (octave_idx_type k = 0; k < m; k++)
        {
          row_id = k * nt;    // row of covaraince matrix

          double ds      = d(k) + d(l);                         // summ of diagonal elements
          bool   ds_zero = (abs (ds) <= eps) ? true : false;    // if the summ is zero we do not calculate

          for (octave_idx_type j = 0; j < nr; j++)
          {
            col = col_id + j;

            /// Time looping ///
            for (octave_idx_type i = 0; i < nt; i++)
            {
              row = row_id + i;

              if (ds_zero)
              {
                // Summ was zero: denominator and numerator are zero, limit is zero
                G(row,col) = 0;
                continue;
              }
              else
              {
                double tl, tm, dk;
                if (t(i) < r(j))
                {
                 tl = t(i);
                 tm = r(j);
                 dk = d(l);
                }
                else
                {
                 tl = r(j);
                 tm = t(i);
                 dk = d(k);
                }

                G(row,col) = (exp (ds * tl) - 1) / ds * exp ( dk * (tm - tl) );
              }

            }
          }  /// en time looping ///

        }
      }    /// end loop over diagonal elements ///
      retval(0) = G;
    }
  return retval;
}

template <class T> void do_cmmult (const Cell & C, const T & M, Cell & B)
{
  dim_vector Bsize = B.dims();
  for (octave_idx_type j = 0; j < Bsize(1); j++)
  {
    for (octave_idx_type i = 0; i < Bsize(0); i++)
    {
      // Fill and assign
      B(i,j) = C(i,0) * M(0,j);

      // Accumulate the rest
      for (octave_idx_type k = 1; k < C.dims()(1); k++)
        B(i,j) = B(i,j) + C(i,k) * M(k,j);
    }
  }
}

// PKG_ADD: autoload ("cmtimes", "covLTI.oct");
DEFUN_DLD (cmtimes, args, nargout,
           "Matrix multiplication of NxM cell and MxK matrix. Returns NxK cell.")
{
  octave_value_list retval;
  int nargin = args.length ();
  if (nargin < 1 || nargin > 2)
    print_usage ();
  else
  {
    bool transposed   = false;
    octave_idx_type c = 0, m = 1;
    if (args(1).is_cell ())
    {
      m = 0;
      c = 1;
      transposed = true;
    }

    Cell C = transposed? args(c).cell_value().transpose () : args(c).cell_value ();
    Cell B;
    if (args(m).is_complex_type ())
    {
      ComplexMatrix M = transposed? args(m).complex_matrix_value().transpose () : args(m).complex_matrix_value ();
      B               = Cell (C.dims()(0), M.dims()(1));
      do_cmmult (C,M,B);
    }
    else
    {
      Matrix M = transposed? args(m).matrix_value().transpose () : args(m).matrix_value ();
      B        = Cell (C.dims()(0), M.dims()(1));
      do_cmmult (C,M,B);
    }

    retval(0) = transposed? B.transpose () : B;
  }
  return retval;
}

/*

%!test
%! Cm = randn (2,3); M = randn(3,3);
%! C = mat2cell (Cm, ones (2,1), ones (1,3));
%! assert (cell2mat(cmtimes (C,M)), Cm*M, sqrt(eps));

%!test
%! Cm = randn (3,2); M = randn(3,3);
%! C = mat2cell (Cm, ones (3,1), ones (1,2));
%! assert (cell2mat(cmtimes (M,C)), M*Cm, sqrt(eps));

%!test
%! Cm = complex(randn (2,3),randn (2,3)); M = complex(randn (3,3),randn (3,3));
%! C = mat2cell (Cm, ones (2,1), ones (1,3));
%! assert (cell2mat(cmtimes (C,M)), Cm*M, sqrt(eps));

%!test
%! Cm = complex(randn (3,2),randn (3,2)); M = complex(randn (3,3),randn (3,3));
%! C = mat2cell (Cm, ones (3,1), ones (1,2));
%! assert (cell2mat(cmtimes (M,C)), M*Cm, sqrt(eps));

*/

// PKG_ADD: autoload ("cctimes", "covLTI.oct");
DEFUN_DLD (cctimes, args, nargout,
           "Matrix multiplication of NxM cell and MxK cell. Returns NxK cell.")
{
  octave_value_list retval;
  int nargin = args.length ();
  if (nargin < 1 || nargin > 2)
    print_usage ();
  else
  {
    Cell A = args(0).cell_value ();
    Cell B = args(1).cell_value ();
    Cell C(A.dims()(0), B.dims()(1));
    do_cmmult (A,B,C);

    retval(0) = C;
  }
  return retval;
}


/*

%!test
%! Cm = randn (2,3); Mm = randn(3,3);
%! C = mat2cell (Cm, ones (2,1), ones (1,3));
%! M = mat2cell (Mm, ones (3,1), ones (1,3));
%! assert (cell2mat(cctimes (C,M)), Cm*Mm, sqrt(eps));

%!test
%! Cm = complex(randn (2,3),randn (2,3)); Mm = complex(randn (3,3),randn (3,3));
%! C = mat2cell (Cm, ones (2,1), ones (1,3));
%! M = mat2cell (Mm, ones (3,1), ones (1,3));
%! assert (cell2mat(cctimes (C,M)), Cm*Mm, sqrt(eps));

*/
