## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## This example requieres GMPL installed
## http://www.gaussianprocess.org/gpml/

clear all, close all

# GP with first order one dimensional prior
u      =@(t,p) 10*exp(-(t-0.4).^2/2/p^2);                  # Actuation with paramter up
up     = 0.05;                        # value of the actuarion parameter

####################
### Problem data ###
T = 1;                              # Time span for data
N = 10;                             # Number of observations
t = linspace (0,T,N).';             # Time grid for observations

a = -3;                             # Decay rate

####################
### Design data ####
y0  = 1;                      # Initial conditions

# SDE
nT = 1e3;                         # Time samples for Ito integration
t_ = linspace (0,T,nT).';           # Time vector for Ito integral
dt = t_(2) - t_(1);               # Time step

s  = 1e-2;              # std of noise
v  = s.^2;
if !exist("y_")
  dW = s/dt * randn (nT,1);         # Wiener differential
  # Ito integral
  y_      = zeros (nT,1);
  y_(1) = y0 + dt*dW(1);
  for i = 1:nT-1
    y_(i+1) = y_(i) + ...
                dt * (y_(i)*a + u(t_(i),up) + dW(i+1));
  endfor

  # Observations
  yo = interp1 (t_,y_,t);   # Create observations from SDE

  # Sample a few trajectory for reference in plots
  dW = s/dt * randn (nT,50);         # Wiener differential
  # Ito integral
  y_      = zeros (nT,50);
  y_(1,:) = y0 + dt*dW(1,:);
  for i = 1:nT-1
    y_(i+1,:) = y_(i,:) + ...
                dt * (y_(i,:)*a + u(t_(i),up) + dW(i+1,:));
  endfor

  #########################
  ### Gaussian process ####
  ## Mean function
  meanfunc = {@meanLTIo1d1_gpml,@(t)u(t,up)};
  hyp.mean = [a; y0];

  ## Covariance function
  covfunc = {@covLTIo1d1_gpml};
  hyp.cov = [a; log(s); log(s)];

  ## Likelihood of observations
  likfunc = @likGauss;
  hyp.lik = log(s);

  nlml = gp (hyp, @infExact, meanfunc, covfunc, likfunc, t, yo)

  [yp s2] = gp (hyp, @infExact, meanfunc, covfunc, likfunc, t, yo, t_);

endif

###############################
### Learning Hyperprameters ###
hyp2.mean = [-1e-1; 0];
hyp2.cov  = [-1e-1; 2; 2];
hyp2.lik  = 2;

# all parameters indepedently
#hyp2  = minimize (hyp2, @gp, -1e3, @infExact, meanfunc, covfunc, likfunc, t, yo);

p = [hyp2.mean; hyp2.lik];
data2hyp =@(x) struct('mean', x(1:2), 'cov',x([1 3]),'lik',x(3));

# using minimize
function [f df] = cost (x, d2h, infer, m, k, l, t, y, i = [])
    [f df] = gp (d2h(x),infer, m, k, l, t, y);
    df = accumarray([1;2;1;3;3], cell2mat(struct2cell(df)), size(x));

    if (i==1)
     f = df;
    end
endfunction
tic
hyp_min  = minimize (p, @cost, -500, data2hyp, @infExact, meanfunc, covfunc, likfunc, t, yo);
hyp_min  = data2hyp(hyp_min);
nlml_min = gp (hyp_min, @infExact, meanfunc, covfunc, likfunc, t, yo)
[yp_min s2_min] = gp (hyp_min, @infExact, meanfunc, covfunc, likfunc, t, yo, t_);
toc

# Using sqp
tic
cost_sqp = @(x)cost(x,data2hyp, @infExact, meanfunc, covfunc, likfunc, t, yo);
#grad_sqp = @(x)cost(x,data2hyp, @infExact, meanfunc, covfunc, likfunc, t, yo, 1);
#[p, OBJ, INFO, ITER, NF, LAMBDA] = sqp (p, {cost_sqp, grad_sqp},[],[],-Inf,[-1e-3 Inf 2], 500);
[p, OBJ, INFO, ITER, NF, LAMBDA] = sqp (p, cost_sqp,[],[],-Inf,[-1e-3 Inf 2], 1000);

hyp_sqp = data2hyp(p);
nlml_sqp = gp (hyp_sqp, @infExact, meanfunc, covfunc, likfunc, t, yo)
[yp_sqp s2_sqp] = gp (hyp_sqp, @infExact, meanfunc, covfunc, likfunc, t, yo, t_);
toc
##############
#### Plots ###
figure (1, 'Name', 'Exact parameters')
plot (t_,[y_ yp]);
axis tight
hp = shadowplot (t_, yp, sqrt(s2));
hold on
h  = plot (t_,y_, '-r', ...
           t, yo, '.b');
legend ([h(1);h(size(y_,2)+1); hp.line.c],{"Simulation", "Obs.","Preds."}, ...
        "Location","SouthWest")

figure (2, 'Name', 'Optimized parameters MIN')
plot (t_,[y_ yp_min]);
axis tight
hp = shadowplot (t_, yp_min, sqrt(s2_min));
hold on
h  = plot (t_,y_, '-r', ...
           t, yo, '.b');
legend ([h(1);h(size(y_,2)+1); hp.line.c],{"Simulation", "Obs.","Preds."}, ...
        "Location","SouthWest")

figure (3, 'Name', 'Optimized parameters SQP')
plot (t_,[y_ yp_sqp]);
axis tight
hp = shadowplot (t_, yp_sqp, sqrt(s2_sqp));
hold on
h  = plot (t_,y_, '-r', ...
           t, yo, '.b');
legend ([h(1);h(size(y_,2)+1); hp.line.c],{"Simulation", "Obs.","Preds."}, ...
        "Location","SouthWest")
