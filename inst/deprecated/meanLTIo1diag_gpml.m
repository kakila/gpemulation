## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## defun
## @end defun

function M = meanLTIo1diag_gpml (m, A, hyp, x, i = [])

  if (nargin < 4)
    M = sprintf ('%d',2*m);
    return
  endif

  if (i > 2*m)
    error ('Only %d hyperparamters!', 2*m);
  end

  x = x(1:length(x)/2);
  A  = cellfun (@(u)feval(u,hyp(1:m)),A, "UniformOutput", false);
  y0 = hyp (m+1:2*m);
  M  = meanLTIo1diag (x, A, y0, i);
  M = cell2mat(M)(:);

endfunction
