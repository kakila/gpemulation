## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defop Method {@@symmat} {@var{b} =} subref (@var{S},@var{idx})
##
## @end defop

function s = subsref (S, idx)

  if (isempty (idx))
    s = S;
  endif

  switch (idx(1).type)

    case "()"
      ind = idx(1).subs;
      if (numel (ind) > 2)
        error ('Octave:invalid-indexing', ...
            "multi-dimensional symmetric matrix not implemented.\n");
      endif
      for i=1:2
        if (ind{i} == ':')
          ind{i} = 1:S.sz(1);
        endif
      endfor

      ni = length (ind{1});
      nj = length (ind{2});
#      if ( ni != nj)
      [ii jj] = meshgrid (ind{1}, ind{2});
      ind = sub2ind_symm (S.sz(1), ii(:), jj(:));
#      else
#        ind = sub2ind_symm (S.sz(1), ind{:});
#      endif
      s = reshape (S.vec(ind), ni, nj);

    otherwise

      error ('Octave:invalid-indexing', ...
             "symmetric matrix cannot be indexed with %c\n", ...
             idx(1).type(1));

  endswitch

  if (numel (idx) > 1)
    b = subsref (S, idx(2:end));
  endif

endfunction

%!shared S
%! S = symmat(1:15);

%!assert (S(1,1), 1)
%!assert (S(:,1), (1:5).');
%!assert (S(:,5), [5 9 12 14 15].');
%!assert (S([4 5], 2), [8; 9]);
%!assert (S(1,:), 1:5);
%!assert (S(5,:), [5 9 12 14 15]);
%!assert (S(2, [4 5]), [8 9]);
