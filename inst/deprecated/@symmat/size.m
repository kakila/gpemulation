## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@symmat} size (@var{S})
## @defmethodx size (@var{S}, @var{DIM})
##
## Return the number of rows and columns of @var{S}.
##
## With one input argument and one output argument, the result is
## returned in a row vector. If there are multiple output arguments,
## the number of rows is assigned to the first, and the number of
## columns to the second, etc.
##
## @seealso{@@symmat/length, @@symmat/numel}
## @end defmethod

function [n, varargout] = size (S, dim = [])

  if (nargin > 2 || (nargout > 1 && dim))
    print_usage ();
  endif

  n = S.sz(1);
  if (dim);
    if (dim >= 3)
      n = 1;
    endif
    return
  endif

  if (nargout <= 1)
    n = [n n];
  elseif (nargout >= 2)
    varargout{1} = n;
    for i=2:nargout-1
      varargout{i} = 1;
    endfor
  endif


endfunction

%!shared S
%! S = symmat(1:15);

%!assert (size (S), [5 5])
%!assert (size (S,1), 5)

%!test
%! [n m c] = size (S);
%! assert([n m c], [5 5 1])
