## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @deftypeop Constructor {@@symmat} {@var{S} =} symmat (@var{A})
##
## Create a symmatric matrix
## @end deftypeop

function S = symmat (A = [])

    if (isa (A, 'symmat'))
      S = A;
      return
    endif

    S = struct ('sz', zeros (1,2), ...
                'vec', []);

    if isvector (A)
      N       = length (A);
      S.sz(2) = N;
      S.sz(1) = (sqrt ( 1 + 8*N ) - 1)/2;
      if (fix (S.sz(1)) != S.sz(1))
        Nn = ((2 * ceil(S.sz(1)) + 1)^2 - 1 ) / 8;
        error ('Octave:invalid-input-arg', ...
        ["The data must fit in a square matrix! " ...
         "The smallest that can contain the given data is of size %d (%d data values missing).\n"], ceil(S.sz(1)), Nn - N)
      endif
      S.vec   = A(:);

    elseif isnumeric (A)
      S.vec = vech (A);
      S.sz  = [length(A) length(S.vec)];
    endif

    S = class (S, 'symmat');

endfunction
