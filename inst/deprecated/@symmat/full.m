## Copyright (C) 2006  Michael Creel <michael.creel@uab.es>
## Copyright (C) 2009  Jaroslav Hajek <highegg@gmail.com>
## Copyright (c) 2011 Juan Pablo Carbajal <carbajal@ifi.uzh.ch>
## Copyright (c) 2011 Carnë Draug <carandraug+dev@gmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @defmethod {@@symmat} {@var{m} =} full (@var{s}, @var{scale})
##
## Returns matrix in full form.
##
## @end defmethod

function M = full (S, scale = 1)
  M = unvech(S.vec, scale);
endfunction

%!assert(full(symmat(eye(3))), eye(3) );

%!test %symmetric
%! dim = 10;
%! A = tril( floor ( 5*(2*rand(dim)-1) ) );
%! A += A.';
%! M = symmat(A);
%! M = full(M, 1);
%! assert (A, M);

%!test %antisymmetric
%! dim = 10;
%! A = tril( floor ( 5*(2*rand(dim)-1) ) );
%! A -= A.';
%! M = symmat(A);
%! M = full(M, -1);
%! assert (A, M);
