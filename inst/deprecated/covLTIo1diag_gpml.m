## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>


function K = covLTIo1diag_gpml (m, A, S, iobs, ipred, hyp, x, z = [], i = [])

  if (nargin < 7)
    K = sprintf ('%d+%s',m, S());
    return
  endif

  if (nargin < 8 || isempty (z)) % whether the two arguments are the same
    z = x;
  end

  A = cellfun (@(u)feval(u,hyp(1:m)),A, "UniformOutput", false);
  S = feval (S, hyp(m+1:end), hyp(1:m),hyp(1:m));

  # FIXME compute only diagonal
#  if (strcmp (z ,'diag') || isempty (z))
#    z = x;
#  endif

#  x  = x(1:length(x)/2);
#  xs = z(1:length(z)/2);
  K = covLTIo1diag (x, z, A, S, iobs, ipred);
  K = cell2mat (K);

  # FIXME compute only diagonal
  if strcmp (z ,'diag')
    K = diag(K);
  endif

endfunction
