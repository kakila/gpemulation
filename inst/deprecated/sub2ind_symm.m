## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {ind = } sub2ind_symm (@var{N}, @var{r}, @var{c})
## @defunx {ind = } sub2ind_symm (@dots{}, @var{num})
##
## Convert subscripts to a linear index of a symmetric matrix of
## size @var{N}.
##
## If @var{num} is true (default false) N is interpreted as the
## number of elements in the packed representation of the matrix.
##
## An example of trinagular matrix linearly indexed follows.
##
## @example
##          N = 4;
##          A = -repmat (1:N,N,1);
##          diagind = 1 + 0.5*(0:N-1).*( (2*N+1):-1:(N+2) );
##          A += repmat (diagind, N,1) - A.';
##          => A =
##              1
##              2    5
##              3    6    8
##              4    7    9   10
## @end example
##
## The following example shows how to convert the two-dimensional
## index `(3,2)' of the 4-by-4 matrix in the previous example to
## a linear index.
##
## @example
##          linear_index = sub2ind (A, 3, 2)
##          => linear_index = 6
## @end example
##
## When @var{r} and @var{c} are row or column matrices of subindeces
## of the same size then @var{ind} have the same shape as any of them.
##
## @seealso{vech, unvech, ind2sub_symm}
## @end defun

function ind = sub2ind_symm (N,r,c, num = false)


  if (nargin < 3)
    print_usage ();
    return
  endif

  if (any(size(r) != size(c)))
    error ('Octave:invalid-input-arg', ...
           "all subscripts must be of the same size\n");
  endif

  R = zeros(size(r));
  C = zeros(size(c));

  below = r >= c;
  above = !below;
  R(below) = r(below);
  C(below) = c(below);

  R(above) = c(above);
  C(above) = r(above);

  if (num)
    N = (sqrt ( 1 + 8*N ) - 1)/2;
  endif
  ind = sub2ind ([N N],R,C) - C.*(C-1)/2;

end

%!shared N, diagind, F, S
%! N = 5;
%! diagind = 1 + 0.5*(0:N-1).*( (2*N+1):-1:(N+2) );
%! F = -repmat (1:N,N,1);
%! F += repmat (diagind, N,1) - F.';
%! F = tril(F) + tril(F,-1).';
%! S = symmat(F);

%!assert (diagind, sub2ind_symm(N,1:N,1:N)) % diagonal indexes

%!test
%! [r c] = ind2sub_symm (N, 1:numel(S));
%! assert (sub2ind_symm (N,r.',c.'), vech (F)) % Full matrix
%! assert (sub2ind_symm (numel(S),r.',c.', true), vech (F)) % Full matrix

%!error sub2ind_symm(N, eye(2), [1 2]); % r is not a row or column matrix
%!error sub2ind_symm(N, [1 2], eye(2)); % c is not a row or column matrix
%!error sub2ind_symm(N, [1 2], [1 2 3]); % c,r not the same size
%!error sub2ind_symm(N, [1 2 3 ], 1); % c,r not the same size
%!error sub2ind_symm(N,{1, 2}, {1, 2}); % idx is not a row or column matrix
%!error sub2ind_symm(N); % not enough arguments
%!error sub2ind_symm(N,[1 2 3]); % not enough arguments
