## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## defun
## @end defun

function [K K0] = covLTIo1d1_gpml (hyp, x, z = [], i = [])

  if (nargin < 2)
    K = '3';
    return
  endif

  if (i > 3)
    error ('Only 3 hyperparamters!');
  end

  if (nargin < 3 || isempty (z)) % whether the two arguments are the same
    z = x;
  end

  a  = hyp(1);                    % decay rate
  s  = exp (2 * hyp(2));          % variance of input noise
  s0 = exp (2 * hyp(3));          % variance of initial condition

  [K K0] = covLTIo1d1 (x, z, a, s, s0, i);

  if (i == 2)
    K *=  2 * s;
  end

  if (i == 3)
    K0 *=  2 * s0;
  end

  K += K0;

endfunction

%!shared t, hyp
%! t  = linspace(0,1,10);
%! hyp = [-1 1 1];

%!test
%! K = covLTIo1d1_gpml ();
%! K = covLTIo1d1_gpml (hyp);

%!test
%! K = covLTIo1d1_gpml (hyp, t);
%! K = covLTIo1d1_gpml (hyp, t,[]);

%!test
%! K = covLTIo1d1_gpml (hyp, t, 'diag');

%!test
%! K = covLTIo1d1_gpml (hyp, t,[],1);
%! K = covLTIo1d1_gpml (hyp, t,[],2);
