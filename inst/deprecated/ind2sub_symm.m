## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {[@var{r} @var{c}] =} ind2sub_symm (@var{N}, @var{i})
## @defunx {[@var{r} @var{c}] =} ind2sub_symm (@dots{}, @var{num})
##
## Return the linear index into packed representation of a
## symmetric matrix of size @var{N}.
##
## If @var{num} is true (default false) N is interpreted as the
## number of elements in the packed representation of the matrix.
##
## @end defun

function [r c] = ind2sub_symm(N,idx, num=false)

  if num
    N = (sqrt ( 1 + 8*N ) - 1)/2;
  endif

  endofrow = 0.5*(1:N) .* (2*N:-1:N + 1);
  c        = lookup (endofrow, idx-1)+1;
  r        = N - endofrow(c) + idx ;

endfunction
