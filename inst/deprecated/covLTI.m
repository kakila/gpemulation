## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @defun {@var{K} =} covLTI (@var{x}, @var{y}, @var{A}, @var{S})
## @defunx {@var{K} =} covLTI (@dots{}, @var{S0})
##
## Return the covariance matrix of the noise coupled LTI system defined by the
## cell @var{A}, with noise with covaraince @var{S}, evaluated at the points
## @var{x}, @var{y}.
##
## If given @var{S0} defines the covaraince of the initial conditions.
## @end defun

function K = covLTI (t, r, A, sigma, sigma0 = [])

  V   = A{1};
  Vi  = A{3};
  d   = A{2};

  VT  = V.';
  ViT = Vi.';
  dT  = d.';

  L0 = 0;
  if !isempty (sigma0)
    # Evolution of the inital conditions
    L0  = (exp (d*t) .* Vi) * sigma0 * (ViT .* exp (dT*r));
  endif

  # Evolution of the noise term
  Lp = Vi * sigma * ViT;
  s  = min (t, r);
  dd = d + dT;
  F = Lp .* exp (d*t + dT*r) .* ( 1 - exp (-dd*s) ) ./ dd;

  tol = sqrt (eps);
  tf = abs(dd) < tol;
  F(tf) = 0;

  # Covariance matrix
  K = V * (L0 + F) * VT;

  # FIXME returns only real cov matrices. Make it optional.
  K = real (K);

endfunction
