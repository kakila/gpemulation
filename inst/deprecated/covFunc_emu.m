## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} covFunc_emu (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

function k = covFunc_emu (covS, covS0, h, x, y=[])
  ## Linear proxy: diagonal first order LTI system
  if isempty(y) || strcmp (y, 'diag')

    S  = feval (covS{:}, h.S, x.nlp);
    S0 = feval (covS0{:}, h.S0, x.ic);
    k = covLTIo1diag ({x.time, x.lp(:,1)}, y, S ,S0, x.idx);

  else

    S  = feval (covS{:}, h.S, x.nlp, y.nlp);
    S0 = feval (covS0{:}, h.S0, x.ic, y.ic);
    k = covLTIo1diag ({x.time, x.lp(:,1)}, {y.time, y.lp(:,1)}, S ,S0, x.idx, y.idx);

  endif
endfunction
