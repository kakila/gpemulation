## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## defun
## @end defun

function M = meanLTIo1d1_gpml (u, hyp, x, i = [])

  if (nargin < 2)
    M = '2';
    return
  endif

  if (i > 2)
    error ('Only 2 hyperparamters!');
  end

  a  = hyp(1);                    % decay rate
  y0 = hyp(2);                    % initial condition

  M = meanLTIo1d1 (x, a, y0, u, i);

endfunction
