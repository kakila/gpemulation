## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @defun {@var{K} =} covLTIo1diag (@var{x})
## @defunx {@var{K} =} covLTIo1diag (@var{x}, @var{y})
## @defunx {@var{K} =} covLTIo1diag (@dots{}, @var{S})
## @defunx {@var{K} =} covLTIo1diag (@dots{}, @var{S}, @var{S0})
## @defunx {@var{K} =} covLTIo1diag (@dots{}, @var{idx})
## @defunx {@var{K} =} covLTIo1diag (@dots{}, @var{idx}, @var{idy})
## @defunx {[@var{K} @var{K0} @var{G} @var{G0}] =} covLTIo1diag (@dots{})
##
## Return the covariance matrix of the noise coupled first order
## LTI system
##
## @tex
## \dot{\boldsymbol{f}}(t) - A \boldsymbol{f}(t) = \boldsymbol{\xi}_g(t) \\
## \dot{\boldsymbol{g}}(t) - B \boldsymbol{g}(t) = \boldsymbol{\xi}_g(t)
## @end tex
## @ifnottex
## @group
## @verbatim
##  df
## ---- = A f(t) + xi_f(t)
##  dt
##
##  dg
## ---- = B f(t) + xi_g(t)
##  dt
##
## @end verbatim
## @end group
## @end ifnottex
##
## with diagonalizable system matrices A and B.
##
## The input @var{x} is a cell. The first element of these cells indicate the @strong{n} time points
## where the covariance is evaluated, i.e. @code{@var{x}@{1@} = t}. The second
## element of the cell can be an array or a cell.
## If it is a cell, it contains  the @strong{m}-by-@strong{m} eigenspace matrix of V,
## its inverse and the array of @strong{m} eigenvalues D of A, i.e. if @code{A = V * diag(D) * inv(V)}
## then @code{@var{x}@{2@} = @{V, D, inv(V)@}}.
## If it is an array, then it is D and the matrix A is considered diagonal.
##
## The behavior of the function depends on the input arguments in the following way:
##
## @table @asis
##
## @item With a single input argument:
## The function computes and returns the covariance matrix where @var{x}@{1@} is
## an array with @strong{n} input cases. The returned covariance matrix is of
## size @strong{nm}-by-@strong{nm}.
##
## @item With two input arguments:
## If @code{@var{y} == 'diag'} then it returns a vector with the @strong{n} self covariances
## for the test cases in @var{x}.
## If @var{y} is a cell, then it containts @strong{k} test cases in its first element
## and the description of the @strong{l}-by-@strong{l} matrix B in its secod element,
## as described above for the input @var{x}.
## In this case the function returns a @strong{nm}-by-@strong{kl} matrix of cross
## covariances between training cases @var{x} and test cases @var{y}.
##
## @end table
##
## The optional input arguments @var{S} and @var{S0} are the covariance matrix of the
## coupling noise and the initial conditions, respectively. If not provided
## then @code{@var{S} = ones(@strong{m},@strong{k})} (fully coupled systems)
## and @code{@var{S0} = eye(@strong{m},@strong{k})} (independent random initial conditions)
##
## The optional input arguments @var{idx} and @var{idy} are used to select only some components
## of the output.
##
## When multiple output argumets are requested the contributions from noisy
## initial conditions is separated. In this case the total covaraince matrix
## is the summ of @var{K} and @var{K0}.
##
## @seealso{covLTIdiago1d1, covGreenLTIo1diag}
## @end defun

function [K varargout] = covLTIo1diag (X, Y=[], S=[], S0=[], iobs=[], ipred=[])

  t = X{1};
  if iscell (X{2})
    V  = X{2}{1};
    d  = X{2}{2}(:); # diagonal as column vector
    Vi = X{2}{3};
  elseif isvector (X{2})
    d = X{2}(:);
    V = Vi = [];
  endif
  Mx = length (d);
  A  = {t,d};

  # If no index are provided, use the whole space
  if isempty (iobs)
    iobs = 1:Mx;
  endif

  flag_diag = false;
  My   = Mx;
  B    = [];
  VyiT = Vi.';
  VyT  = V.';
  if strcmp (Y,'diag')
    flag_diag = true;
    B         = 'diag';
    ipred     = iobs;
  elseif ~isempty (Y)
    r = Y{1};
    if iscell (Y{2})
      VyT  = Y{2}{1}.';
      dy   = Y{2}{2}(:); # diagonal as column vector
      VyiT = Y{2}{3}.';
    elseif isvector (Y{2})
      dy = Y{2}(:);
      VyT = VyiT = [];
    endif
    My = length (dy);
    B = {r,dy};
  endif

  if (isempty (S))
    S = ones (Mx,My);
  endif

  if (isempty (S0))
    S0 = eye (Mx,My);
  endif

  if (isempty (ipred) && isempty(Y)) # If no index are provided, use the whole observed space
     ipred = iobs;
  elseif isempty (ipred)
    ipred = 1:My;
  endif
  Mo = length (iobs);
  Mp = length (ipred);

  # Each cell element corresponds to a diagonal element pair
  if (~isempty (V))
    S    = Vi * S * VyiT;
    S0   = Vi * S0 * VyiT;
  endif
  if (nargout > 1)
    [G G0] = covGreenLTIo1diag (A, B, S, S0);
  else
    G = covGreenLTIo1diag (A, B, S, S0);
  endif

  [nt nr] = size(G{1});
  if flag_diag
    #FIXME too slow! Try to use the reshape-permute trick
    if (~isempty (V))
      VT = V.';
      K  = arrayfun (@(i)cell2mat (cmtimes(V(i,:), cmtimes(G,VT(:,i)))), ipred, "unif", 0)(:);
      K  = real (cell2mat (K));
    else
      K = arrayfun (@(i)G{i,i}, ipred, "unif", 0)(:);
      K = cell2mat (K);
    endif
    if (nargout > 1)
      if (~isempty (V))
        K0 = arrayfun (@(i)cell2mat (cmtimes(V(i,:), cmtimes(G0,VT(:,i)))), ipred, "unif", 0)(:);
        K0 = real (cell2mat (K0));
      else
        K0 = arrayfun (@(i)G0{i,i}, ipred, "unif", 0)(:);
        K0 = cell2mat (K0);
      endif
    endif

  else
    K = cell2mat (G);
    K = reshape (K, [nt Mx nr My]);
    K = permute (K, [2 1 3 4]);
    K = reshape (K, [Mx*nt*nr My]);
    if (~isempty (V))
      K = V(iobs,:) * reshape (K * VyT(:,ipred), [Mx nt*nr*Mp]);
    else
      K = reshape (K(:,ipred), [Mx nt*nr*Mp])(iobs,:);
    endif
    K = reshape (K, [Mo nt nr Mp]);
    K = permute (K, [2 1 3 4]);
    K = real(reshape (K, [nt*Mo nr*Mp]));
    if (nargout > 1)
      K0 = cell2mat(G0);
      K0 = reshape (K0, [nt Mx nr My]);
      K0 = permute (K0, [2 1 3 4]);
      K0 = reshape (K0, [Mx*nt*nr My]);
      if (~isempty (V))
        K0 = V(iobs,:) * reshape (K0 * VyT(:,ipred), [Mx nt*nr*Mp]);
      else
        K0 = reshape (K0(:,ipred), [Mx nt*nr*Mp])(iobs,:);
      endif
      K0 = reshape (K0, [Mo nt nr Mp]);
      K0 = permute (K0, [2 1 3 4]);
      K0 = real(reshape (K0, [nt*Mo nr*Mp]));
    endif
  endif

  if (nargout > 1)
    varargout(1:3) = {K0,G,G0};
  endif

endfunction

%!shared t, r, n, m, A, B, k,l
%! n = 10;
%! t  = linspace(0,1,n);
%! k = 5;
%! r  = linspace(0,1,k);
%! A  = [-1 -3 -5];
%! m = length (A);
%!
%! M     = [0 1; -(2*pi*10)^2 0.1*4*pi*10];
%! [V,D] = eig (M);
%! l     = length (D);
%! B     = {V, diag(D), inv(V)};

%!test
%! K = covLTIo1diag ({t,A}, 'diag');
%! assert (size(K), [n*m, 1])

%!test
%! K = covLTIo1diag ({t,A});
%! assert (size(K), [n*m, n*m])
%! K = covLTIo1diag ({t,A}, {r,B});
%! assert (size(K), [n*m, k*l])

%!test
%! K = covLTIo1diag ({t,A}, {r,B}, [],[], 1:2, 1);
%! assert (size(K), [n*2, k])

%!test
%! K = covLTIo1diag ({t,A}, {r,B}, [],[], 3, 2);
%! assert (size(K), [n, k])

%!test
%! Kt = covLTIo1diag ({t,A});
%! [K K0] = covLTIo1diag ({t,A});
%! assert (all (Kt == K + K0))

%!test
%! [K K0 G G0] = covLTIo1diag ({t,A});

%!demo
%! t = linspace (0,1,100).';
%! r = 0.5;
%! a = [-1 -5];
%! K = covLTIo1diag ({t,a}, {r,a});
%! K = covmat2cell (K, [length(t) length(a) length(r) length(a)]);
%!
%! K = K.';
%! for i=1:4
%!   subplot (2,2,i);
%!   plot (t, K{i}, 'linewidth', 2);
%!   if any(i == [3 4]); xlabel ('Time'); endif;
%!   if any(i == [1 3]); ylabel ('Covariance'); endif;
%!   [k,l] = ind2sub([2 2],i);
%!   title (sprintf("Component %d with %d",l,k));
%! endfor
%! axis tight
%!
%! #################################################
%! # Covariance functions for a first order system with two components.

%!demo
%! t = linspace (0,1,100).';
%! r = 0.5;
%!
%! # Oscillator
%! M     = [0 1; -(2*pi*5)^2 -0.1*4*pi*5];
%! [V d] = eig (M);
%! d     = diag (d);
%! Vi    = inv (V);
%! S     = ones (2);
%! K = covLTIo1diag ({t,{V,d,Vi}}, {r,{V,d,Vi}}, S, zeros(2));
%! Kc = covmat2cell (K, [length(t) length(d) length(r) length(d)]);
%!
%! # SDE
%! pkg load statistics
%! nT = 1e3;                         # Time samples for Ito integration
%! t_ = linspace (0, 1, nT);         # Time vector for Ito integral
%! dt = t_(2) - t_(1);               # Time step
%! eA = expm (M * dt);               # Evolution matrix
%! # Ito integral
%! n  = 5e2;
%! y_ = zeros (nT,n,2);
%! for j=1:n
%!   dW = mvnrnd (0, dt * S, nT).';   # Wiener differential
%!   y_(1,j,:) = [1; 0];
%!   for i = 1:nT-1
%!     y_(i+1,j,:) = eA * squeeze(y_(i,j,:)) + dW(:,i+1);
%!   endfor
%! endfor
%!
%! # Experimental covariance
%! eK  = zeros (nT,4);
%! ym =  mean (y_, 2);
%! dy = y_ - ym;
%! for i=1:2
%!   for j = 1:2
%!     eK(:,(i-1)*2+j)  = (dy(:,:,i) * dy(:,:,j).')(:,nT/2) / n;
%!   endfor
%! endfor
%!
%!
%! Kc = Kc.';
%! for i=1:4
%!   subplot (2,2,i);
%!   h = plot (t, Kc{i}, t_, eK(:,i));
%!   set (h(1), 'linewidth', 2);
%!   axis tight
%!   if any(i == [3 4]); xlabel ('Time'); endif;
%!   if any(i == [1 3]); ylabel ('Time'); endif;
%!   [k,l] = ind2sub ([2 2],i);
%!   title (sprintf("Component %d with %d",l,k));
%! endfor
%!
%! #################################################
%! # Covariance functions for a first order system with companion matrix
%! # compared with the ones calculated from samples.

%!demo
%! t = linspace (0,1,100).';
%! r = 0.5;
%!
%! # Oscillator
%! M     = [0 1; -(2*pi*5)^2 -0.01*4*pi*5];
%! [V d] = eig (M);
%! d     = diag (d);
%! # First order system
%! a = -5;
%! # combined eigenspace
%! V  = blkdiag (1,V);
%! Vi = inv (V);
%! d  = [a;d];
%! # coupling
%! S = [1e-6 0 1e-2; 0 1e-6 0; 1e-2 0 1e2];
%!
%! tmp      = cell(1,4);
%! K        = covLTIo1diag ({t,{V,d,Vi}},{r,{V,d,Vi}}, S, zeros(3), 1, 2:3);
%! tmp(1:2) = covmat2cell (K, [length(t) 1 length(r) 2]);
%! K        = covLTIo1diag ({t,{V,d,Vi}},{r,{V,d,Vi}}, S, zeros(3), 2:3, 1);
%! tmp(3:4) = covmat2cell (K, [length(t) 2 length(r) 1]);
%! K        = cell2mat(tmp);
%!
%!
%! for j=1:4
%!   subplot(2,2,j)
%!   h = plot (t_, k(:,j),'-r',...
%!             t, K(:,j),'-k');
%!   set (h,'linewidth', 2);
%!   ylabel ('Covariance [a.u.]')
%!   if (j ==1); legend (h, "Experimental", "Exact"); endif
%!   axis tight
%!   if (j <= 2)
%!    title (sprintf("Components 1 and %d",j+1));
%!   else
%!    title (sprintf("Components %d and 1",j));
%!   endif
%! endfor
%! xlabel ('Time');
%! #################################################
%! # Comparing covariance calculated from n samples and theoretical calculation.

%!demo
%! t = linspace (0,1,100).';
%! r = 0.5;
%!
%! # Oscillator
%! M     = [0 1; -(2*pi*5)^2 -0.1*4*pi*5];
%! [V d] = eig (M);
%! d     = diag (d);
%! # First order system
%! a = -5;
%! # combined eigenspace
%! V  = blkdiag (1,V);
%! Vi = inv (V);
%! d  = [a;d];
%! # coupling
%! S = [1 0 1; 0 1 0; 1 0 1];
%!
%! K = covLTIo1diag ({t,{V,d,Vi}},{r,{V,d,Vi}}, S, zeros(3));
%! K = covmat2cell (K, [length(t) length(d) length(r) length(d)]);
%!
%! K = K.';
%! for i=1:9
%!   subplot (3,3,i);
%!   plot (t, K{i}, 'linewidth', 2);
%!   axis tight
%!   if any(i == 7:9); xlabel ('Time'); endif;
%!   if any(i == [1 4 7]); ylabel ('Cov'); endif;
%!   [k,l] = ind2sub([3 3],i);
%!   title (sprintf("Component %d with %d",l,k));
%! endfor
%!
%! #################################################
%! # Covariance functions for a first order system
%! # coupled with the velocity of a second order system.

%!demo
%! t = linspace (0,1,100).';
%! r = 0.5;
%!
%! # Oscillator
%! M     = [0 1; -(2*pi*5)^2 -0.01*4*pi*5];
%! [V d] = eig (M);
%! d     = diag (d);
%! # First order system
%! a = -5;
%! # combined eigenspace
%! V  = blkdiag (1,V);
%! Vi = inv (V);
%! d  = [a;d];
%! # coupling
%! S = [1e-6 0 1e-2; 0 1e-6 0; 1e-2 0 1e2];
%!
%! tmp      = cell(1,4);
%! K        = covLTIo1diag ({t,{V,d,Vi}},{r,{V,d,Vi}}, S, zeros(3), 1, 2:3);
%! tmp(1:2) = covmat2cell (K, [length(t) 1 length(r) 2]);
%! K        = covLTIo1diag ({t,{V,d,Vi}},{r,{V,d,Vi}}, S, zeros(3), 2:3, 1);
%! tmp(3:4) = covmat2cell (K, [length(t) 2 length(r) 1]);
%! K        = cell2mat(tmp);
%!
%! # SDE
%! pkg load statistics
%! nT = 1e3;                         # Time samples for Ito integration
%! t_ = linspace (0, 1, nT);         # Time vector for Ito integral
%! dt = t_(2) - t_(1);               # Time step
%! eA = expm (blkdiag (a,M) * dt); # Evolution matrix
%! # Ito integral
%! n  = 5e2;
%! y_ = zeros (nT,n,3);
%! for j=1:n
%!   dW = mvnrnd (0, dt * S, nT).';   # Wiener differential
%!   y_(1,j,:) = [1; 1; 0];
%!   for i = 1:nT-1
%!     y_(i+1,j,:) = eA * squeeze(y_(i,j,:)) + dW(:,i+1);
%!   endfor
%! endfor
%!
%! # Experimental covariance
%! k  = zeros (nT,4);
%! ym =  mean (y_, 2);
%! dy = y_ - ym;
%! for i=1
%!   for j = 2:3
%!     k(:,j-1)  = (dy(:,:,i) * dy(:,:,j).')(:,nT/2) / n;
%!   endfor
%! endfor
%!
%! for i=2:3
%!   for j = 1
%!     k(:,i+1)  = (dy(:,:,i) * dy(:,:,j).')(:,nT/2) / n;
%!   endfor
%! endfor
%!
%! for j=1:4
%!   subplot(2,2,j)
%!   h = plot (t_, k(:,j),'-r',...
%!             t, K(:,j),'-k');
%!   set (h,'linewidth', 2);
%!   ylabel ('Covariance [a.u.]')
%!   if (j ==1); legend (h, "Experimental", "Exact"); endif
%!   axis tight
%!   if (j <= 2)
%!    title (sprintf("Components 1 and %d",j+1));
%!   else
%!    title (sprintf("Components %d and 1",j));
%!   endif
%! endfor
%! xlabel ('Time');
%! #################################################
%! # Comparing covariance calculated from n samples and theoretical calculation,
%! # for a mixed system.


%!demo
%! nT = 100;
%! t = linspace (0,1,nT).';
%!
%! # Oscillator
%! w     = 2*pi*5;
%! M     = [0 1; -(w)^2 -0.1*2*w];
%! [V d] = eig (M);
%! Vi    = inv (V);
%! d     = diag (d);
%!
%! # First order system
%! a = -3;
%!
%! # combined eigenspace
%! V  = blkdiag (1,V);
%! Vi = blkdiag (1,Vi);
%! d  = [a;d];
%!
%! # coupling
%! S  = ones(3);                        # Covariance of noise
%! S0 = zeros(3);                       # Covariance of inital conditions
%! K  = covLTIo1diag ({t,{V,d,Vi}}, [], S, S0);
%!
%! subplot (3,6,[1:3 7:9 13:15])
%! colormap (cubehelix);
%! imagesc (K);
%! set(gca,"visible","off");
%! hold on
%! line(nT*[0 1; 3 1], nT*[1 0; 1 3]);
%!
%! K  = covmat2cell (K, [length(t) length(d)]);
%! ax = [4 10 16 5 11 17 6 12 18];
%! for i=1:9
%!   subplot (3,6,ax(i));
%!   [k,l] = ind2sub([3 3],i);
%!   y = K{i}(:,nT/2);
%!   plot(t,y,"linewidth", 2);
%!   set(gca,"visible","off");
%!
%!   title (sprintf("%d-%d",k,l));
%! endfor
