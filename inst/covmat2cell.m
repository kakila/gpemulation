## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{C} =} covmat2cell (@var{K}, @var{s})
## Converts a covariance matrix @var{K} to a cell.
##
## The vector @code{@var{s} = [nT m nR n]} indicates the size of the covariance matrix. @var{K} is
## assumed to have the size structure (nT m)-by-(nR n), where nT and nR are number
## of time samples, and m and n are the size of the phase spaces.
## If @var{s} has two elmeents only then nR=nT and n=m.
##
## The returned cell has size m-by-n each cell element a matrix of size nT-by-nR.
##
## @seealso{mat2cell}
## @end deftypefn

function K = covmat2cell (K, siz)

  switch length(siz)
    case 4
      K = mat2cell (K, siz(1) * ones (siz(2),1), siz(3) * ones (1, siz(4)));
    case 2
      K = mat2cell (K, siz(1) * ones (siz(2),1), siz(1) * ones (1, siz(2)));
  endswitch

endfunction

%!shared nT, nR, n, m, K, TR, Ks, TRs
%! n  = 2;
%! m  = 3;
%! nT = 5;
%! nR = 7;
%! [J I] = meshgrid (1:nR,1:nT);
%! TR    = I * 1e1 + J;
%! TRe   = repmat (TR, n, m);
%!
%! [J I] = meshgrid (1:m,1:n);
%! G     = I * 1e3 + J * 1e2;
%! Ge    = kron (G, ones (nT,nR));
%! K     = Ge + TRe;
%!
%! [J I] = meshgrid (1:nT);
%! TRs    = I * 1e1 + J;
%! TRe   = repmat (TRs, n, n);
%!
%! [J I] = meshgrid (1:n);
%! Gs     = I * 1e3 + J * 1e2;
%! Ge     = kron (Gs, ones (nT));
%! Ks     = Ge + TRe;

%!test
%! Kc = covmat2cell (K, [nT n nR m]);
%! assert (size(Kc),[n m]);
%! assert (size(Kc{1}),[nT nR]);

%!test
%! Kc = covmat2cell (Ks, [nT n]);
%! assert (size(Kc),[n n]);
%! assert (size(Kc{1}),[nT nT]);

%!test
%! Kc = covmat2cell (K, [nT n nR m]);
%! for j=1:m
%!  for i=1:n
%!    assert (Kc{i,j},TR+i*1e3+j*1e2);
%!  endfor
%! endfor

%!test
%! Kc = covmat2cell (Ks, [nT n]);
%! for j=1:n
%!  for i=1:n
%!    assert (Kc{i,j},TRs+i*1e3+j*1e2);
%!  endfor
%! endfor

%!demo
%! n  = 2;
%! m  = 3;
%! nT = 4;
%! nR = 5;
%! [J I] = meshgrid (1:nR,1:nT);
%! TR    = I * 1e1 + J;
%! TRe   = repmat (TR, n, m);
%!
%! [J I] = meshgrid (1:m,1:n);
%! G     = I * 1e3 + J * 1e2;
%! Ge    = kron (G, ones (nT,nR));
%! K     = Ge + TRe;
%! Kc    = covmat2cell (K, [nT n nR m]);
%!
%! i = 1; j = 2;  k = 3; l=4;
%! printf ("K{%d,%d}(%d,%d) = %d\n",i,j,k,l,Kc{i,j}(k,l));
%!
%! #########################################################
%! # Any element K{i,j}(k,l) is ijkl
