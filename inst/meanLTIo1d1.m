## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @defun {@var{K} =} meanLTIo1d1 (@var{x}, @var{a}, @var{y0}, @var{s})
## @defunx {@var{K} =} meanLTIo1d1 (@dots{}, @var{i})
##
## Return the mean function of the first order LTI system
##
## @tex
## \dot{f}(t) = a f(t) + \xi(t)
## @end tex
## @ifnottex
## @group
## @verbatim
##  df
## ---- = a f(t) + xi(t)
##  dt
## @end verbatim
## @end group
## @end ifnottex
##
## with noise with covariance @var{s}, evaluated at the points
## @var{x}.
##
##
## If @var{i} is given, the function returns the partial derivatives
## of the mean vector w.r.t. @var{a} (@code{@var{i}==1}) and @var{y0} (@code{@var{i}==2})
##
## The mean function does not depend on the covariance @var{s}.
##
## @end defun

function M = meanLTIo1d1 (t, a, y0, u, i = [], method='fft')

  #FIXME maxF should be a parameter

  t  = t(:);    # first argument is rows
  if (isempty (i))
    M  = y0 * exp (a * t);
  elseif (i == 1) % derivative wrt a
    M = t .* (y0 * exp (a * t));
  elseif (i == 2) % derivative wrt y0
    M = exp (a * t);
  end

  if strcmp (method , 'fft');

    if (~isempty (u) && (isempty(i) || i ~= 2))
      T    = max (t);
      maxF = 1e3; # in Hz
      dt   = 1/ ( 2 * maxF);
      nT   = 2 ^ nextpow2 (T / dt);
      t_   = linspace (min (t), T, nT).';
      n    = 2 ^ nextpow2 (2*nT - 1);

      if (isempty (i))
        H  = fft ((t_ == 0) * 0.5 + (t_ > 0) .* exp (a*t_), n);
      elseif (i == 1) % derivative wrt a
        H  = fft ((t_ > 0) .* t_ .* exp (a*t_), n);
      end

      %yp = 4 * sqrt(2*pi) * real (ifft (H .* fft (u(t_), n))(1:nT)) / nT;
      yp = T * real (ifft (H .* fft (u(t_), n))(1:nT)) / nT;
      M += interp1 (t_, yp, t);
    end

  elseif strcmp (method, 'lsode')

    if (~isempty (u) && (isempty(i) || i ~= 2))

      if (isempty (i))
        M = lsode (@(x,z)a * x + u(z), y0, t);
      elseif (i == 1) % derivative wrt a
        M = t .* lsode (@(x,T)a * x + u(T), y0, t) - ...
                      (lsode (@(x,T)a * x + T.*u(T), y0, t) - y0 * exp (a * t));
      end

    end

  end % if over methods

endfunction

%!demo
%! a  = -1;
%! t  = linspace (0,sqrt(71),64).';
%! y0 = 1;
%!
%! A  = [-5 10 4 2];
%! s2 = 0.1*[0.01 0.025 0.05 0.2].^2;
%! t0 = t(end)*[0.05 0.15 0.4 0.8];
%! u  = @(t) sum( A .* exp(-(t(:)-t0).^2./s2) , 2);
%!
%! t0 = cputime;
%! m1 = meanLTIo1d1 (t, a, y0, u, i = [], 'fft');
%! printf ('FFT: Elapsed time %g\n', cputime-t0);
%!
%! t0 = cputime;
%! m2 = meanLTIo1d1 (t, a, y0, u, i = [], 'lsode');
%! printf ('LSODE: Elapsed time %g\n', cputime-t0);
%!
%! figure(1)
%! clf
%! subplot (1,1,1)
%! t_ = linspace(0,t(end),1e3);
%! h2 = plot(gca, t_, u(t_), '-k');
%! axis tight
%! set(gca, 'box','off', 'yaxislocation','right', 'xaxislocation','top');
%! ylabel ('Input');
%! ax = axes ('position', get(gca,'position'));
%! h1 = plot (ax, t, m1, '.r', t, m2, 'ob');
%! set(ax,'box', 'off', 'color', 'none');
%! axis tight
%! xlabel ("Time");
%! ylabel ("Response");
%! legend ([h1; h2], {'fft','lsode','input'})

%!shared a, t, y0, u
%! a = -5;
%! t = linspace (0,1,10).';
%! y0 = 1;
%! u = @(t) 2*exp(-(t-0.1).^2/0.2^2) + exp(-(t-0.3).^2/0.3^2) + 5*exp(-(t-0.8).^2/0.1^2);

%!test
%! m1 = meanLTIo1d1 (t, a, y0, u, i = [], 'fft');
%! m2 = meanLTIo1d1 (t, a, y0, u, i = [], 'lsode');
%! assert(m1,m2, 5e-4);
