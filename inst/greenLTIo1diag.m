## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

function G = greenLTIo1diag (t, r, A, adj = false)

  t  = t(:);     # first argument is rows
  r  = r(:).';   # second argument is columns

  if iscell (A)
    V  = A{1};
    d  = A{2}(:).'; # diagonal as row vector
    Vi = A{3};
  elseif isvector (A)
    d = A(:).';
    V = [];
  endif

  dT = t - r;
  if adj
    dT = -dT;
  endif
  [nt nr] = size(dT);

  Z  = double (abs (dT) <= eps);
  H  = double (dT > 0);
  G  = arrayfun (@(u) H .* exp (u * dT) + Z .* 0.5 , d, 'UniformOutput', false);

  if ~isempty (V)
    m = length (d);
    if adj
      tmp = V.';
      V = Vi.';
      Vi = tmp;
      clear tmp;
    endif

    Vic = mat2cell (Vi, ones(m,1), m);
    K   = cell2mat(cellfun(@kron, Vic, G.', 'UniformOutput', false));
    K   = reshape (K, [nt m nr m]);
    K = permute (K, [2 1 3 4]);
    K = reshape (K, [m  nt*nr*m]);
    K = V * K;
    K = reshape (K, [m nt nr m]);
    K = permute (K, [2 1 3 4]);

    G = real(reshape (K, [nt*m nr*m]));
  endif

endfunction

%!demo
%! nT = 1e3;
%! t = linspace (0, 1, nT).';
%! r = 0.5;
%! w = 2*pi*3;
%! c = 0.1;
%! d = [-3 -5 -w*(c+sqrt(c^2-1)) -w*(c-sqrt(c^2-1))];
%!
%! G  = greenLTIo1diag (t, r, d);
%! Gt = greenLTIo1diag (t, r, d, true);
%!
%! figure(1)
%! clf
%! subplot(2,1,1)
%! hold on
%! for i = 1:length(G)
%!   plot(t, real(G{i}),'-r');
%!   plot(t, real(Gt{i}),'-b');
%! endfor
%! hold off
%! subplot(2,1,2)
%! hold on
%! for i = 1:length(G)
%!   plot(t, imag(G{i}),'-r');
%!   plot(t, imag(Gt{i}),'-b');
%! endfor
%! hold off

%!demo
%! nT = 1e3;
%! t  = linspace (0, 1, nT).';
%! dt = t(2) - t(1);
%! r  = 0.5;
%!
%! w     = 2*pi*3;
%! c     = 0.1;
%! M     = [0 1; -w^2 -2*c*w];
%! [V d] = eig (M);
%! d     = diag (d);
%! Vi    = inv (V);
%!
%! G = greenLTIo1diag (t, r, {V,d,Vi});
%! G = covmat2cell (G, [nT 2 1 2]);
%! u = @(t)exp(-(t-0.4).^2/0.04);
%!
%! yp(:,1) = conv(G{1,2}, u(t), "same") * dt;
%! yp(:,2) = conv(G{2,2}, u(t), "same") * dt;
%! yr = lsode(@(x,t)M*x + [0;1]*u(t), [0 0], t);
%!
%! subplot(2,1,1)
%! plot(t, [yp(:,1) yr(:,1)]);
%! subplot(2,1,2)
%! plot(t, [yp(:,2) yr(:,2)]);
