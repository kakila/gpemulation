## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

# This script shows the matching between the calculated matrices and the ones
# obtained directly from sampling the SDE.

pkg load statistics
# SDE
nT = 5e2;                         # Time samples for Ito integration
t_ = linspace (0, 1, nT);         # Time vector for Ito integral
dt = t_(2) - t_(1);               # Time step

a = [-1; -3];

eA = diag(exp(a*dt)); # Evolution matrix
S  = ones(2);  # Coupling

tic
if ~exist('y_','var')
  # Ito integral
  n         = 5e2;
  y_        = zeros (nT,n,2);
  y_(1,:,:) = 1;
  for j = 1:n
    dW = mvnrnd (0, dt * S, nT).';   # Wiener differential
    for i = 1:nT-1
      y_(i+1,j,:) = eA * squeeze(y_(i,j,:)) + dW(:,i+1);
    endfor
  endfor
endif
toc

tic
# Experimental covariance
ym =  mean (y_, 2);
dy = y_;
for j = 1:n
  dy(:,j,:) = y_(:,j,:) - ym;
endfor
k = cell (2);
for i=1:2
  for j=1:2
    k{i,j}  = (dy(:,:,i) * dy(:,:,j).')(:,nT/2) / n;
  endfor
endfor
toc

tic
nT = 1e2;
t  = linspace (t_(1),t_(end), nT).';
K  = covGreenLTIo1diag ({t,a}, {0.5,a}, S, zeros(2));
toc

k = horzcat(k{:});
K = horzcat(K{:});
he = plot (t_, k);
hold on
ht = plot (t, K);
hold off
set(ht, 'linewidth', 2);
cellfun(@(x,c)set(x,'color',c), mat2cell(ht,ones(length(ht),1),1), get(he,'color'));
axis tight
legend([ht(1) he(1)],{'Exact','Experimental'})
xlabel('Time');
ylabel('Covariance');
