## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

function k = covFunc_emu (covS, covS0, h, x, y=[])

  n = length(x.nlp);
  X = {x.time, param2matrix(x.lp)};
  idxx = [];
  if ~isempty (x.idx)
   idxx = cell2mat (arrayfun ( ...
          @(i)(i-1)*2 + x.idx, 1:n, ...
          "unif", 0));
  endif

  if isempty (y) || strcmp (y, 'diag')

    S  = feval (covS{:}, h.S, x.nlp);
    S0 = feval (covS0{:}, h.S0, x.ic);
    S  = kron (S, eye (2));
    S0 = kron (S0, eye (2));
    k  = covLTIo1diag (X, y, S ,S0, idxx);

  else
    ny = length(y.nlp);
    Y  = {y.time, param2matrix(y.lp)};
    idxy = [];
    if ~isempty (y.idx)
      idxy = cell2mat (arrayfun ( ...
           @(i)(i-1)*2 + y.idx, 1:ny, ...
           "unif", 0));
    endif

    S  = feval (covS{:}, h.S, x.nlp, y.nlp);
    S0 = feval (covS0{:}, h.S0, x.ic, y.ic);
    S  = kron (S, eye (2));
    S0 = kron (S0, eye (2));
    k  = covLTIo1diag (X, Y, S ,S0, idxx, idxy);

  endif
endfunction

