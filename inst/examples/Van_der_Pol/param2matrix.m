## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

function A = param2matrix (lp)
  # Egiven-values and -vector functions
  Df  =@(l) l.*[-J; J];                 # Eigenvalues
  Vf  =@(l) [ones(1,2); Df(l).'];       # Eigenvectors
  Vif =@(l) 0.5 * [ones(2,1) 1./Df(l)]; # Inverse of Eigenvectors

  # Eigenspace
  V    = arrayfun (Vf, lp, "unif", 0);
  V(1) = sparse (V{1});
  V    = blkdiag (V{:});
  # Inverse eigenspace
  Vi    = arrayfun (Vif, lp, "unif", 0);
  Vi(1) = sparse (Vi{1});
  Vi    = blkdiag (Vi{:});
  # Eigenvalues
  D = cell2mat (arrayfun (Df, lp, "unif", 0));

  A = {V,D,Vi};
endfunction
