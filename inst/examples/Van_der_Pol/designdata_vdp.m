## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

#######################
##  Data generation  ##
#######################
nT  = 40;                            # Number of time samples
n   = 200;                           # Number of design datasets

T   = 30;                             # Time to wait before observation
t   = linspace (0, 15, nT).';         # Time samples
mu  = linspace (0.3, 7, n)';          # The oscillator parameters
d   = 2;                              # Dimension of the simulator signals

vdp = @(y,t, mu) [y(2); ...                          # Simulator is
                  mu * (1 - y(1)^2) * y(2) - y(1)];  # Van der Pol oscillator

y0        = [0.5 0];                                   # Initialization conditions
simulator = @(t,p) lsode (@(x,t)vdp(x,t,p), ...
        lsode(@(x,t)vdp(x,t,p), y0, [0 T])(end,:), ... # Initial condition
                    t);

y    = zeros (nT, n, d);              # Matrix to store the design datasets

nz = 10;
z  = zeros (nT*3, nz, d);
iz = 0;
idx = zeros(1,nz);
tz = linspace (0,15,3*nT);
for i=1:n
  y(:,i,:) = simulator (t, mu(i));
  if (mod(i,8) == 0 || i == 1)
    iz += 1;
    idx(iz) = i;
    z(:,iz,:) = simulator (tz,mu(i));
  endif
endfor

p   = 1;           # Dimension of simulator parameter vector
nlp = mu;          # Simulator parameter matrix

#############
##  Plots  ##
#############
figure (1);
clf
c = jet (nz);

# Phase space
subplot(4,4,[1 2 5 6]);
h = plot (z(:,:,1), z(:,:,2));
arrayfun(@(i)set (h(i), "color", c(i,:), "linewidth", 2), 1:nz);
set(gca, "yaxislocation", "right");
axis tight
grid on

# X coordinate
subplot(4,4,[9 10 13 14]);
h = plot (z(:,:,1), tz);
arrayfun(@(i)set (h(i), "color", c(i,:), "linewidth", 2), 1:nz);
ylabel ("time")
set (gca, "ydir", "reverse", "xaxislocation", "top");
axis tight
grid on

# Y coordinate
subplot(4,4,[3 4 7 8]);
h = plot (tz,z(:,:,2));
arrayfun(@(i)set (h(i), "color", c(i,:), "linewidth", 2), 1:nz);
xlabel ("time")
set(gca, "xaxislocation", "top");
axis tight
grid on

# Legend
subplot(4,4,[11 12 15 16]);
p   = get (gca, "position");
txt = arrayfun(@(x)num2str(round(x*1e2)*1e-2), mu(idx), "unif", 0);
h   = legend (h, txt);
set(h, "visible", "off", "position", p);
set (gca, "visible", "off");
title ("Parameter value")

clear z nz tz idx iz
