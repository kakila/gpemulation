## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} icovFunc_emu (@var{}, @var{})
## Inverse of the covaraince function at inputs
##
## @seealso{}
## @end deftypefn

function k = icovFunc_emu (A, covS, h, x)
  ## FIXME Use inverse of S!
  ## FIXME How to deal with noise on IC?
  ## Sherman-Morrison-Woodbury update?
  n = length(x.nlp);
  t = x.time;

  # Eigenspace
  V  = arrayfun (A{1}, x.lp, "unif", 0);
  # Inverse eigenspace
  Vi = arrayfun (A{3}, x.lp, "unif", 0);
  # Eigenvalues
  D  = arrayfun (A{2}, x.lp, "unif", 0);

  S  = feval (covS{:}, h.S, x.nlp);
  S  = kron (S, ones(length(D{1})));

  G  = cellfun(@(x,y,z)greenLTIo1diag (t, t, {x,y,z}), V, D, Vi, "unif", 0);
  Gt = cellfun(@(x,y,z)greenLTIo1diag (t, t, {x,y,z}, 1), V, D, Vi, "unif", 0);

  iG  = blkdiag(cellfun(@inv, G, "unif", 0){:});
  iGt = blkdiag(cellfun(@inv, Gt, "unif", 0){:});

  dt = t(2) - t(1);
  k  = iGt * iG / dt;
endfunction
