## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>


# Load datasets
fname = "vanderpol.dat";
if ~exist(fname,'file')
 source('proxies_vdp.m')
endif
ddata = load (fname);

vdp = @(y,t, mu) [y(2); ...                          # Simulator is
                 mu * (1 - y(1)^2) * y(2) - y(1)];   # Van der Pol oscillator

y0        = [0.5 0];                                 # Initialization conditions
simulator = @(t,p) cell2mat(
                   arrayfun (
       @(r)lsode (@(x,t)vdp(x,t,r), ...
       lsode(@(x,t)vdp(x,t,r), y0, [0 ddata.T])(end,:), ... # Initial condition
                   t)(:,1), ...
                   p(:).', 'unif', 0));

##################
#### Emulator ####
##################
proxy = @(x) x.ic(:,1).' .* cos (x.lp.' .* x.time) + ...
             (x.ic(:,2)./x.lp).' .* sin (x.lp.' .* x.time); # Proxy

pkg load gpml

mdp      = log (min (tril (abs (ddata.nlp - ddata.nlp.'),-1,'pack')));
covfunc  = {@covFunc_emu, {@covMaterniso,1},{@covSEiso}};
meanfunc = {proxy};

mdp     = log (min (tril (abs (ddata.nlp - ddata.nlp.'),-1,'pack')));
hyp.S   = [mdp+log(2) 0];
hyp.S0  = [mdp+log(1) 0];
#     covfunc  = {@covFunc_emu, {@covSEiso},{@covSEiso}};
#     hyp.S   = [mdp-log(7.5) 0];
#     hyp.S0  = [mdp-log(3) 0];

meanfunc = {proxy};

emu     = gpemulator ('covFunc', {covfunc{:},hyp}, ...
                     'meanFunc', meanfunc, ...
                     'regParam', 1e-6);

#### Condition the emulator on design data
par = ddata.parmap (ddata.nlp);
n   = size (ddata.y, 2);
nic = min (max (round (0.1*n), 120), n);
x   = struct ('time', ddata.t, ...
             'nlp', ddata.nlp, ...
             'lp', par(:,1), ...
             'ic', par(:,2:3), ...
             'idx', 1, ...
             'include', randperm (n,nic));

emu = designdata (emu, x, ddata.y(:,:,1));
#emu = designdata (emu, x, ddata.y(:,:,1),"fitc");

#### Emulate
nT  = 2 * length (ddata.t);
t   = linspace (ddata.t(1), ddata.t(end)+2, nT).';
N   = 5;
nls = [min(ddata.nlp) max(ddata.nlp)];
nlp = linspace (nls(1)-2*exp(mdp), nls(2), N).';
Y   = simulator (t, nlp);

par = ddata.parmap (nlp);
x   = struct ('time', t, ...
             'nlp', nlp, ...
             'lp', par(:,1), ...
             'ic', par(:,2:3), ...
             'idx', 1);
yp  = emulate (emu, x);

figure (1);
clf
subplot(2,1,1)
c = jet (N);
h = plot (t,Y,'.','markersize', 9);
arrayfun (@(i)set(h(i),"color",c(i,:)), 1:N);
hold on
hp = plot (t,yp);
arrayfun (@(i)set(hp(i),"color",c(i,:)), 1:N);
hold off
axis tight
xlabel ('time')
ylabel ('x')
legend ([h(1) hp(1)], {'Simulation','Emulation'},'Location','Northoutside', ...
'orientation','horizontal')

subplot(2,1,2)
nlp = nls(2)+exp(mdp);
Y   = simulator (t, nlp);
par = ddata.parmap (nlp);
x   = struct ('time', t, ...
             'nlp', nlp, ...
             'lp', par(:,1), ...
             'ic', par(:,2:3), ...
             'idx', 1);
yp  = emulate (emu, x);
h = plot (t,Y,'.r','markersize', 9);
hold on
hp = plot (t,yp,'-k');
hold off
axis tight
xlabel ('time')
ylabel ('x')
legend ([h(1) hp(1)], {'Simulation','Emulation'})
