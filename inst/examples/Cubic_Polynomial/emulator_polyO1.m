## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
##
## Emulation of 1st order dynamical system with polynomial nonlinearities.
## the dynamical system is defined with the equations
## @tex
## $$
##  \dot{x} = f(t,x) = -x \left( x - 1\right)\left( x - 2\right) + u(t)
## $$
## @end tex
## @ifnottex
## @group
## @verbatim
##
##  dx
## ---- = f(t,x) = a x + b  ( (x - x1) (x - x2) (x - x3) ) + u(t)
##  dt
##
## @end verbatim
## @end group
## @end ifnottex
##
## To build the Gaussian Processes the emulator uses @math{n} coupled linear
## proxies of the form,
## @tex
## $$
##  \dot{x}_i = a_i x_i + u(t) + n(t)
## $$
## @end tex
## @ifnottex
## @group
## @verbatim
##
##  dx[i]
## ------ = a[i] x[i] + u(t) + n(t)
##   dt
##
## @end verbatim
## @end group
## @end ifnottex
## Where @math{n(t)} is white noise with covariance matrix depending on the
## vector of parameters on whic emulation will be performed. In this case
## we emulate the response of the system for different initial conditions.

pkg load gpml

#########################
#### Design datasets ####
fname = "cubicpoly.dat";
if ~exist (fname, "file")
  generate_cubicpoly
endif
ddata = load (fname);

###########################
#### Parameter mapping ####
yx  = ddata.y([1:end-1],:);
dyx = diff (ddata.y)([1:end],:) / (ddata.t(2) - ddata.t(1));

pp = cell2mat (arrayfun (@(i)polyfit(yx(:,i), dyx(:,i), 1), (1:ddata.N).', 'unif', 0));

# Generate a mapping from simulator to emulator parameters
pp = interp1 (ddata.nlp, pp, 'pp');
parammap = @(nlp) ppval (pp, nlp);

##################
#### Emulator ####
function k = covFunc_emu (covS, covS0, h, x, y=[])
  ## Linear proxy: diagonal first order LTI system
  if isempty(y) || strcmp (y, 'diag')

    S  = feval (covS{:}, h.S, x.nlp);
    S0 = feval (covS0{:}, h.S0, x.ic);
    k = covLTIo1diag ({x.time, x.lp(:,1)}, y, S ,S0, x.idx);

  else

    S  = feval (covS{:}, h.S, x.nlp, y.nlp);
    S0 = feval (covS0{:}, h.S0, x.ic, y.ic);
    k = covLTIo1diag ({x.time, x.lp(:,1)}, {y.time, y.lp(:,1)}, S ,S0, x.idx, y.idx);

  endif
endfunction

covfunc  = {@covFunc_emu, {@covSEiso},{@covSEiso}};
meanfunc = {@(x)lsode(@(z,t_)x.lp(:,1).*z + x.lp(:,2),x.ic,x.time)};

mdp     = log (min (tril (abs (ddata.nlp - ddata.nlp.'),-1,'pack')));
hyp.S   = [mdp+log(1) 0];
hyp.S0  = [mdp-1 0];

emu     = gpemulator ('covFunc', {covfunc{:},hyp}, ...
                      'meanFunc', meanfunc, ...
                      'regParam', 0);

# Condition the emulator
printf ("Conditioning...\n");
fflush (stdout);
tic
lp  = parammap (ddata.nlp);
x   = struct ('time', ddata.t, ...
              'nlp', ddata.nlp, ...
              'lp', lp, ...
              'ic', ddata.y(1,:).', ...
              'idx', []);

emu = designdata (emu, x, ddata.y);
toc

# Emulate
printf ("Creating reference trajectories...\n");
fflush (stdout);
nT = 3 * length (ddata.t);
t  = linspace (ddata.t(1), ddata.t(end)+1, nT).';
tic
N   = 20;
nls = [min(ddata.nlp) max(ddata.nlp)];
nlp  = linspace (nls(1)-0.05, nls(2)+0.05, N).';
Y   = ddata.simulator (t, nlp);  # Reference trajectories nT-by-N-by-m
toc

printf ("Emulation...\n");
fflush (stdout);
tic
lp  = parammap (nlp);
x   = struct ('time', t, 'nlp', nlp, 'lp', lp, 'ic', nlp, 'idx', []);
yp  = emulate (emu, x);
toc


###############
#### Plots ####
## Check how the desing data is approximated
y = insample (emu);

printf ("Linf error %g %g\n", max (abs(y-ddata.y)(:)), max (abs(yp-Y)(:)))

figure(1,'Name', 'Approximation of design data');
h = plot (ddata.t, ddata.y,':', ddata.t, y,'.');
C = get(h(1:ddata.N), "color");
arrayfun (@(x)set (h(x+ddata.N), "color", C{x},"markersize", 9), 1:ddata.N);
axis tight

figure(2,'Name', 'Predictions');
h = plot (t, Y,'-', t, yp,'--', ddata.t, y,':k');
C = get(h(1:N), "color");
arrayfun (@(x)set (h(x+N), "color", C{x}), 1:N);
axis tight

figure(3,'Name', 'Nonlinearity');
dy = ( diff (yp) / (t(2) -t(1)) );
x  = linspace (min(ddata.y(:)), max(ddata.y(:)), 100).';
dx = ddata.simulator([], x);
h = plot (yp(2:end,:), dy,'.',x, dx,'-k');
line([x(1) x(end)],[0 0]);
axis tight
