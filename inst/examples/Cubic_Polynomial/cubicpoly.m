## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deffn cubicpoly (@var{t}, @var{x0})
## Integrates a dynamical system defined by dx/dt = p(x),
## with p(x) a cubic polynomial.
##
## @var{t} is the timestamps vector where the solution will be evaluted
## @end deffn

function y = cubicpoly (t, x0, int=[])
  pf   = [-0, -50, 0.25, 0.5, 0.65];
  func =@(t,x) pf(1) * x(:) + pf(2) * (x(:) - pf(3)) .* (x(:) - pf(4)) .* (x(:) - pf(5));

  if ~isempty (t)
    opt  = odeset ('RelTol', 1e-6, ...
                    'AbsTol', 1e-6, ...
                    'InitialStep', 1e-3, ...
                    'MaxStep', t(end) / 10);

    if isempty (int)
      [t_ y] = ode45 (func, t, x0(:), opt);
    else
      [t_ y] = ode45 (func, t([1 end]), x0(:), opt);
      y = interp1 (t_, y, t);
    endif

  else
    y = func(0,x0(:));
  endif
endfunction
