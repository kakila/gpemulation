## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} generate_cubicpoly (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

clear all
v  = version (); v = strsplit (v, "."); v = cellfun (@str2num,v, "unif", 0);
tf = cellfun (@isempty, v); v(tf) = []; v = cell2mat (v); flag_old = false;
if (sum(v) < 5);  flag_old = true; pkg load odepkg; endif
clear v

nT  = 100;
t   = linspace (0, 2, nT).';
N   = 20;                         # Number of design datasets
y0  = [linspace(0.15, 0.45, 3) linspace(0.46, 0.55, 14) linspace(0.56, 0.75, 3)].'; # Initial conditions are the nl parameters

if flag_old
  simulator = @(t,p) cubicpoly(t,p, 1);
else
  simulator = @cubicpoly;
endif

y         = simulator (t, y0);
fname     = "cubicpoly.dat";
nlp       = y0;

save (fname);
