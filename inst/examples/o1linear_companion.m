## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
# GP associated with a 1st order 1D ODE
# dx/dt = A*x + xi
# where xi is zero mean gaussian and std s
# In this case the matrix A is a 2x2 companion matrix
# which implements a second order system (a damped harmonic oscillator)
# with frequency sqrt(w2).

# It is possible to estimate the position/velocity by observing
# the other state.
# Try idx_obs = 1 (observation of position) and idx_pred = [1 2] (estimate position and velocity),
# and
# idx_obs = 2; idx_pred = [2 1];

clear all
pkg load odepkg statistics

# GP with first order prior
u      =@(t,p) [zeros(length(t),1) exp(-(t(:)-0.45).^2/0.05^2)]; # Actuation with paramter up
up     = 0;                        # value of the actuarion parameter
prior  =@(t,x,a,up) a*x + u(t,up); # The linear dynamical system

####################
### Problem data ###
T = 1;                              # Time span for data
N = 2;                              # Number of observations
t = linspace (0,T,N);               # Time grid for observations

w2 = (2*pi)^2*30;                   # Base frequency of oscillator
c  = 2*0.075*sqrt(w2);              # Damping
A  = [0 1; -w2 -c];                 # Companion matrix
m  = size (A,2);                    # Phase space dimension
D  = eig (A);                       # Eigenvalues
V  = (D.^[1:m]).';                  # Eigenvectors
Ac = {V,D,inv(V)};                  # Input for cov function

####################
### Design data ####
y0  = [1 0];                      # Initial conditions

# SDE
nT = 1e3;                         # Time samples for Ito integration
t_ = linspace (0,T,nT);           # Time vector for Ito integral
dt = t_(2) - t_(1);               # Time step

eA = real((Ac{1}.*exp(Ac{2}.'*dt))*Ac{3}).'; # Evolution matrix

S  = diag([0 1e1]).^2;       # Covariance matrix

# Ito integral
dW      = mvnrnd (0, dt * S, nT);   # Wiener differential
y_      = zeros (nT,m);
y_(1,:) = y0 + dW(1,:);
for i = 1:nT-1
  y_(i+1,:) = y_(i,:) * eA + ...
              u(t_(i),up) + dW(i+1,:);
endfor

# Observations
idx_obs  = 1;                        # Index of the observed states
idx_pred = [1 2];                    # Index of states to estimate
mp = length (idx_pred);
yo = interp1 (t_,y_,t)(:,idx_obs);   # Create observations from SDE

#########################
### Gaussian process ####
## Mean function
ym  = meanLTIo1diag (t_, Ac, y0(:), @(t)u(t,up), idx_pred); # Mean func at prediction
ymo = meanLTIo1diag (t , Ac, y0(:), @(t)u(t,up), idx_obs);  # Mean func at observations

## Covariance function
Koo = covLTIo1diag ({t,Ac},[],S,S, idx_obs);                # Obs. block
Kto = covLTIo1diag ({t_,Ac},{t,Ac},S,S, idx_pred, idx_obs); # Pred.-Obs. block
Ktt = covLTIo1diag ({t_,Ac},'diag',S,S, idx_pred);          # Pred. block (only for variance)

l    = sqrt (eps) * eye (size (Koo)); # regularization parameter
l   += diag (diag (Koo));
iKoo = cholinv (Koo + l);

dyo = yo - ymo;                                           # Residuals
yp  = reshape (Kto * iKoo * dyo(:), nT, mp) + ym;         # Prediction on t_
dyp = reshape (Ktt - diag(Kto * iKoo * Kto.'), nT, mp);   # Variance
dyp = 1.96 * sqrt (dyp);                                  #95% confidence

#############
### Plots ###
# Sample a trajectory for reference
ns = 10;
y_      = zeros (nT,m,ns);
for j=1:ns
  # Ito integral
  dW = mvnrnd (0, dt * S, nT); # Wiener differential
  y_(1,:,j) = y0 + dW(1,:);
  for i = 1:nT-1
    y_(i+1,:,j) = y_(i,:,j) *  eA + ...
                + u(t_(i),up) + dW(i+1,:);
  endfor
endfor

fig = figure (1);
clf;
subplot (2,1,1);
plot (t_, [squeeze(y_(:,idx_pred(1),:)) yp(:,idx_pred(1))]);
axis tight
h  = plot (t_, squeeze (y_(:,idx_obs(1),:)), '-r',  "linewidth", 1);
hold on
hp = shadowplot (t_, yp(:,idx_pred(1)), dyp(:,idx_pred(1)));
ho = plot (t, yo(:,1), '.b', "markersize", 12);
legend ([h(1);ho(1); hp.line.c],{"Simulation", "Obs.","Preds."}, ...
        "Location","SouthWest")
hold off

subplot (2,1,2)
plot (t_,[squeeze(y_(:,idx_pred(2),:)) yp(:,idx_pred(2))]);
axis tight
h  = plot (t_, squeeze (y_(:,idx_pred(2),:)), '-r', "linewidth", 1);
hold on
hp = shadowplot (t_,yp(:,idx_pred(2)),dyp(:,idx_pred(2)));
if length(idx_obs) == 2
plot (t, yo(:,2), '.b',"markersize",12);
endif
U = u(t_)(:,2);
v = axis ();
U = (v(4)-v(3)).*((U - min(U)) ./ (max(U) - min(U))) + v(3);
hu = plot (t_,U, '-g', "linewidth", 1);
hold off
