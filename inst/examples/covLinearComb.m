## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

# This script shows the matching between the calculated matrices and the ones
# obtained directly from sampling the SDE.

pkg load statistics
# SDE

a = -5:0.5:-0.5;
a = a;
m = length (a);

C  = 5*randn(1,m); C /= sum (C);# read-out

tic
if ~exist('y_','var')
  nT = 5e2;                         # Time samples for Ito integration
  t_ = linspace (0, 1, nT);         # Time vector for Ito integral
  dt = t_(2) - t_(1);               # Time step

  eA = diag(exp(a*dt)); # Evolution matrix
  V  = orth (randn(m));
  S  = V*diag(rand(m,1))*inv(V);  # Coupling

  # Ito integral
  n         = 5e2; # trajectoires
  y_        = zeros (nT,n,m);
  y_(1,:,:) = 1;
  for j = 1:n
    dW = mvnrnd (0, dt * S, nT).';   # Wiener differential
    for i = 1:nT-1
      y_(i+1,j,:) = eA * squeeze(y_(i,j,:)) + dW(:,i+1);
    endfor
  endfor
  y_ = reshape (reshape (y_, nT*n,m) * C.', nT, n);

  # Experimental covariance
  ym =  mean (y_, 2);
  dy = y_ - ym;
  k = (dy * dy.')(:,nT/2) / n;

endif
toc


tic
nT = 1e2;
t = linspace (t_(1),t_(end), nT).';
K = covGreenLTIo1diag ({t,a}, {0.5,a}, S, zeros(m));
K = cell2mat(cmtimes (C,cmtimes (K, C.')));
toc

he = plot (t_, k);
hold on
ht = plot (t, K);
hold off
set(ht, 'linewidth', 2);
legend([ht(1) he(1)],{'Exact','Experimental'})
xlabel('Time');
ylabel('Covariance');
axis tight
