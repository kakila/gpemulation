## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

nT = 1e2;
nR = 1e3;
t  = linspace (0,1,nT).';
r  = linspace (0,1,nR);
dr = r(2) - r(1);

w     = 2*pi*3;
c     = 0.1;
M     = [0 1; -w^2 -2*c*w];
[V d] = eig (M);
d     = diag (d);
Vi    = inv (V);
m     = 2;

#d     = [-3 -8];
#V     = eye(2) + randn(2);
#Vi    = inv(V);
#m     = 2;

S  = eye (m);
S0 = 1e-5*eye (m);

G = greenLTIo1diag (t, r, {V,d,Vi});
G = reshape (G, [nT*m*nR m]);
G = G * S;
G = reshape (G, [nT*m nR*m]);

Gt = greenLTIo1diag (r, t, {V,d,Vi},1);

Kapp  = G * Gt * dr;
Kappc = covmat2cell (Kapp,[nT m]);

K  = covLTIo1diag ({t,{V,d,Vi}},[],S,S0);
Kc = covmat2cell (K,[nT m]);

cellfun(@(x,y)norm(x-y)./norm(y), Kappc, Kc)

