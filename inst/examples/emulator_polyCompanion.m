## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
##
## Emulation of 2nd order dynamical system with polynomial nonlinearities.
## the dynamical system is defined with the equations
## @tex
## $$
##  \dot{x} = f(t,x) = -x \left( x - 1\right)\left( x - 2\right) + u(t)
## $$
## @end tex
## @ifnottex
## @group
## @verbatim
##
##  dx
## ---- = f(t,x) = a x + b  ( (x - x1) (x - x2) (x - x3) ) + u(t)
##  dt
##
## @end verbatim
## @end group
## @end ifnottex
##
## To build the Gaussian Processes the emulator uses @math{n} coupled linear
## proxies of the form,
## @tex
## $$
##  \dot{x}_i = a_i x_i + u(t) + n(t)
## $$
## @end tex
## @ifnottex
## @group
## @verbatim
##
##  dx[i]
## ------ = a[i] x[i] + u(t) + n(t)
##   dt
##
## @end verbatim
## @end group
## @end ifnottex
## Where @math{n(t)} is white noise with covariance matrix depending on the
## vector of parameters on whic emulation will be performed. In this case
## we emulate the response of the system for different initial conditions.

clear all;

############################
### Problem & Prior data ###
w2 = (2*pi)^2*30;                    # Base frequency of oscillator
c  = 2*0.1*sqrt(w2);                # Damping
A  =@(p) [0 1; -p];                 # Companion matrix
m  = 2;                    # Phase space dimension

## System to emulate
pf = 2.0;
dynsys = @(t, x,p) A(p(1:2))*x + [0;1] .* (-p(1)*pf*x(1,:).^3);

# Initial conditions
N  = 20;                                # Numer of replicas == Number of design datasets
x0 = [linspace(0.8, 2, N).' zeros(N,1)];

#########################
#### Design datasets ####
#pkg load odepkg
T  = 1;                     # Time interval
n  = 25;                    # Number of time samples
to = linspace (0, T, n).';  # Sampled times

opt = odeset ('RelTol', 1e-6, ...
                  'AbsTol', 1e-6, ...
                  'InitialStep', 1e-3, ...
                  'MaxStep', T / 10);
ppf = cell(1,N);
for i=1:N
  [t_ y_] = ode45 (@(t,x)dynsys(t,x,[w2 c]), [0 T], x0(i,:), opt);
  ppf{i}  = interp1 (t_,y_,'pp');
endfor
yf = @(t)permute(reshape(cell2mat(cellfun(@(p)ppval(p,t),ppf,"unif",0)),length(t),m,N),[1 3 2]);
yo = yf(to);

##################
#### Emulator ####
## Mapping from nonlinear model parametres to proxy parameters
w02 = zeros(N,1);
for i = 1:N
  if yo(1,i,1) < 0.5
    w02(i) = -polyfit(yo(:,i,1).', dynsys(0,squeeze(yo(:,i,:)).',[w2 c])(2,:)+c*yo(:,i,2).', 3)(3);
  else
    w02(i) = -polyfit(yo(:,i,1).', dynsys(0,squeeze(yo(:,i,:)).',[w2 c])(2,:)+c*yo(:,i,2).', 1)(1);
  endif
endfor
parammap = interp1 (x0(:,1), w02, 'pp');

# observed parameters
p = [ppval(parammap, x0(:,1)) repmat(c,N,1)];

pkg load gpml
function y = meanFunc(x)
  A  =@(p) [0 1; -p(1:2)]; # Companion matrix
  pc = mat2cell (x.lp, ones(size(x.lp,1),1), size(x.lp,2));
  # Eigenspace
  A = cellfun (A, pc, "unif", 0);
  A(1) = sparse (A{1});
  A    = blkdiag (A{:});

  if (size(x.lp,2) == 5)
    y = lsode(@(z,t_)A*z + x.lp(:,5),x.lp(:,3:4).'(:),x.time);
  else
    y = lsode(@(z,t_)A*z,x.lp(:,3:4).'(:),x.time);
  end
  y = permute(reshape (y, length(x.time), 2, size(x.lp,1)),[1 3 2]);
  y = y(:,:,x.idx);
endfunction

function k = covFunc (h, x, y=[])
  y_flag = ~isempty(y) && ~strcmp (y, 'diag');

  nlpy = lpy = icy = [];
  if y_flag
    nlpy = y.nlp;
    lpy  = y.lp;
    icy  = lpy(:,3:4);
  endif
  S  = covSEiso (h.S,x.nlp,nlpy);
  S  = kron (S, eye(2));
  S0 = covSEiso (h.S0,x.lp(:,3:4),icy);
  S0 = kron (S0, eye(2));

  # Egiven-values and -vector functions
  Df  =@(l) (-l(2) + [-1; 1] * sqrt(l(2)^2 - 4*l(1))) / 2; # Eigenvalues
  Vf  =@(l) [ones(1,2); Df(l).'];  # Eigenvectors
  Vif =@(l) [[flipud(Df(l)).*[1;-1]] [-1; 1]] ./ sqrt(l(2)^2 - 4*l(1));
  %[[1/2*[-1;1].*l(1)] [-1;1]]/sqrt(l(2)^2-4*l(1)) + [1/2 0; 1/2 0];# Inverse of Eigenvectors

  pc = mat2cell (x.lp, ones(size(x.lp,1),1), size(x.lp,2));
  # Eigenspace
  V    = cellfun (Vf, pc, "unif", 0);
  V(1) = sparse (V{1});
  V    = blkdiag (V{:});
  # Inverse eigenspace
  Vi    = cellfun (Vif, pc, "unif", 0);
  Vi(1) = sparse (Vi{1});
  Vi    = blkdiag (Vi{:});
  # Eigenvalues
  D = cell2mat (cellfun (Df, pc, "unif", 0));

  idxx = cell2mat (arrayfun (@(i)(i-1)*2 + x.idx, 1:numel(pc), "unif", 0));
  x    = {x.time, {V, D, Vi}};

  idxy = [];
  if y_flag
    pc = mat2cell (y.lp, ones(size(y.lp,1),1), size(y.lp,2));
    # Eigenspace
    V    = cellfun (Vf, pc, "unif", 0);
    V(1) = sparse (V{1});
    V    = blkdiag (V{:});
    # Inverse eigenspace
    Vi    = cellfun (Vif, pc, "unif", 0);
    Vi(1) = sparse (Vi{1});
    Vi    = blkdiag (Vi{:});
    # Eigenvalues
    D    = cell2mat (cellfun (Df, pc, "unif", 0));
    idxy = cell2mat (arrayfun (@(i)(i-1)*2 + y.idx, 1:numel(pc), "unif", 0));
    y    = {y.time, {V,D,Vi}};
  endif

  k = covLTIo1diag (x, y, S ,S0, idxx, idxy);
endfunction

mdx = log(min(tril(abs(x0(:,1)-x0(:,1).'),-1,'pack')));

hyp.S   = [mdx+0.0 0];
hyp.S0  = [mdx-1 0];

emu = gpemulator ('covFunc', {@covFunc, hyp}, ...
                  'meanFunc', {@meanFunc}, ...
                  'regParam', 0);

printf ("Conditioning...\n");
fflush (stdout);
tic
clear x;
x.time = to;
x.lp   = [p x0];
x.nlp  = x0;
x.idx  = 1;
emu = designdata (emu, x, yo(:,:,x.idx));
toc
###################
#### Emulation ####
## unseen data
Nn     = 1;
dx0    = (max(x0(:,1))-min(x0(:,1)))*rand(Nn,1)/N;
x0n    = x0(randi(N,Nn,1),:) + dx0 .* [1 0];
t      = unique ([linspace(0,T+2*(to(2)-to(1)),300).';to]);  # Timestamps to emulate
pn     = [ppval(parammap,x0n(:,1)) repmat(c,Nn,1)];
pp     = pn; %[p; pn]; # Parameter values to emulate
x0p    = x0n; %[x0; x0n];

## prediction
printf ("Predicting...\n");
fflush (stdout);
tic
clear x;
x.time  = t;
x.lp    = [pp x0p];
x.nlp   = x0p;
x.idx   = [1 2];
[y dy ym Kpo] = emulate (emu, x);
toc

##############
#### Plot ####
z = yf(t)(:,:,1);
printf ("Reference trajectories...\n");
fflush (stdout);
tic
zn = zeros (length(t),Nn);
for i=1:Nn
  [t_ z_] = ode45 (@(t,x)dynsys(t,x,[w2 c]), t([1 end]), x0n(i,:), opt);
  zn(:,i) = interp1 (t_,z_(:,1),t);
endfor
toc

if size(y,2) == Nn
  printf ("Linf error %g %g\n", NA, max (abs(y(:,1:Nn,1)-zn)(:)))
else
  printf ("Linf error %g %g\n", max (abs(y(:,1:end-Nn,1)-z)(:)), max (abs(y(:,end-Nn+1:end,1)-zn)(:)))
endif

figure (1)
clf
h = plot (t,y(:,:,1),'-g',"linewidth",2);
hold on
%hm = plot (t,ym(:,:,1),':b');
hd = plot (t,z,':k');
hr = plot (t,zn,'-k');
ho = plot (to,yo(:,:,1),'.r');
hold off
axis tight
legend ([hr(1),ho(1), h(1)],{'Actual','Design data','Prediction'});
ylabel ('State');
xlabel ('Time');

figure(2)
clf
plot(to,yo(:,:,1),'.r',to, mean(emu)(:,:,1),'-g');
axis tight
