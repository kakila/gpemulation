## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

# This scripts compares the calulated matrices with ones obtained with
# numerical integration.

clear all

nT = 50;
t  = linspace (0,1,nT).';
tc = mat2cell (t, ones(nT,1), 1);
r  = 0.5;

a  = [-1 -3];
A  = diag(a);
AT = A.';
m  = size (A,2);
S  = ones(m);

func = @(tau,t,r,i,j) arrayfun (@(z)expm (A * (t-z))(i,:) * S * expm (AT * (r-z))(:,j), tau);

k = zeros (length (t));
tic
tf  = t > r;
k = cell (m);
for j=1:m
  for i=1:m
    tmp = cellfun (@(x)quadgk (@(z)func(z,x,r,i,j) ,t(1), r), tc(tf));
    k{i,j}(tf) = tmp;
    tmp = cellfun (@(x)quadgk (@(z)func(z,r,x,j,i) ,t(1), x), tc(~tf));
    k{i,j}(~tf) = tmp;%flipud(tmp);
  endfor
endfor
toc

tic
K = covLTIo1diag ({t,a},{r,a},S, zeros(size(A)));
toc

figure (1)
clf
for i=1:m^2
  subplot (m,m,i);
  plot(t,k{i},'.r',t, K{i},'-k');
  axis tight
endfor
hold off
