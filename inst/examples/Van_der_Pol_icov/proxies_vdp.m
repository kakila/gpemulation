## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

#######################
##  Data generation  ##
#######################
if ~exist ("y","var")
  source('designdata_vdp.m');
endif
n = size (y, 2);

proxy = @(t,p) p(2) * cos (p(1) * t) + p(3)/p(1) * sin (p(1) * t); # Proxy structure
q     = 3;                                    # the paramters (q = 2)

z     = zeros (nT, n, 1);                     # Matrix to store the proxies
Q     = zeros (n, q);                         # Matrix of proxy parameters

tmp2 = [1 0 0.5];
for i=1:n
  tmp2   = sqp (tmp2, @(x)sumsq(y(:,i,1)-proxy(t,x)));
  Q(i,:) = tmp2;
  z(:,i) = proxy (t, tmp2);
endfor

# Mapping
pp     = interp1 (mu, Q, "pp");
parmap = @(x) ppval(pp, x);

fname = "vanderpol.dat";
save (fname,"nlp", "t", "y","parmap", "T");

#############
##  Plots  ##
#############
#figure (1);
#clf

## Fits
#subplot (2,1,1)
#h = plot (t, y(:,1:10:n,1),'.');
#c = jet (length (h));
#arrayfun(@(i)set (h(i), "color", c(i,:), "markersize", 6), 1:length(h));
#hold on
#hp = plot (t, z(:,1:10:n), '-');
#hold off
#arrayfun(@(i)set (hp(i), "color", c(i,:), "linewidth", 2), 1:length(hp));
#xlabel ("time")
#ylabel ("x")
#axis tight
##grid on
#legend ([h(1) hp(1)],{'Dataset','Proxy'},"location","northoutside","orientation", "horizontal")

## interpolated params
#subplot (2,1,2)
#dp = abs(mean(diff(sort(nlp))));
#nl = linspace (min(nlp)+dp/2, max(nlp)-dp/2, 3).';
#ll = parmap (nl);
#yr = zeros (nT,3,2);
#zi = zeros (nT,3);
#for i =1:3
#  yr(:,i,:) = simulator(t, nl(i));
#  zi(:,i) = proxy(t,ll(i,:));
#endfor
#h = plot(t,yr(:,:,1),'o');
#c = jet (length (h));
#hold on
#arrayfun(@(i)set (h(i), "color", c(i,:), "markersize", 6), 1:length(h));
#hp = plot(t,zi,'-');
#arrayfun(@(i)set (hp(i), "color", c(i,:), "linewidth", 2), 1:length(hp));
#hold off
#axis tight
#legend ([h(1) hp(1)],{'Reference','Int. Proxy'},"location","northoutside","orientation", "horizontal")

#figure (2)
#plot(nl,ll,'.', nlp, Q,'o');
