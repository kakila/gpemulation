## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>


# Load datasets
fname = "vanderpol.dat";
if ~exist(fname,'file')
  source('proxies_vdp.m')
endif
ddata = load (fname);

vdp = @(y,t, mu) [y(2); ...                          # Simulator is
                 mu * (1 - y(1)^2) * y(2) - y(1)];   # Van der Pol oscillator

y0        = [0.5 0];                                 # Initialization conditions
simulator = @(t,p) cell2mat(
                   arrayfun (
       @(r)lsode (@(x,t)vdp(x,t,r), ...
       lsode(@(x,t)vdp(x,t,r), y0, [0 ddata.T])(end,:), ... # Initial condition
                   t)(:,1), ...
                   p(:).', 'unif', 0));

##################
#### Emulator ####
##################
proxy = @(x) x.ic(:,1).' .* cos (x.lp.' .* x.time) + ...
             (x.ic(:,2)./x.lp).' .* sin (x.lp.' .* x.time); # Proxy

pkg load gpml
Df        =@(l) l.*[-J; J];                 # Eigenvalues
Vf        =@(l) [ones(1,2); Df(l).'];       # Eigenvectors
Vif       =@(l) 0.5 * [ones(2,1) 1./Df(l)]; # Inverse of Eigenvectors

covfunc  = {@icovFunc_emu, 0, {Vf,Df,Vif}, {@covMaterniso,1}};
meanfunc = {proxy};

icovfunc  = {@icovFunc_emu, 1, {Vf,Df,Vif},{@covMaterniso,1}};

mdp     = log (min (tril (abs (ddata.nlp - ddata.nlp.'),-1,'pack')));
%hyp.S   = [mdp+log(2) 0];
hyp.S   = [mdp-log(12) 0];

emu     = gpemulator ('covFunc', {covfunc{:},hyp}, ...
                      'icovFunc', {icovfunc{:},hyp}, ...
                     'meanFunc', meanfunc, ...
                     'regParam', 0);

#### Condition the emulator on design data
par = ddata.parmap (ddata.nlp);
x   = struct ('time', ddata.t, ...
             'nlp', ddata.nlp, ...
             'lp', par(:,1), ...
             'ic', par(:,2:3), ...
             'idx', 1);
emu = designdata (emu, x, ddata.y(:,:,1), "chol");

#### Emulate
nT  = length (ddata.t);
t   = linspace(ddata.t(1), ddata.t(end), nT).';
N   = size(ddata.nlp,1);
nls = [min(ddata.nlp) max(ddata.nlp)];
nlp = ddata.nlp; %linspace (nls(1)-2*exp(mdp), nls(2), N).';
Y   = simulator (t, nlp);

par = ddata.parmap (nlp);
x   = struct ('time', t, ...
             'nlp', nlp, ...
             'lp', par(:,1), ...
             'ic', par(:,2:3), ...
             'idx', 1);
yp  = emulate (emu, x);

figure (1);
clf
subplot(2,1,1)
c = jet (N);
h = plot (t,Y,'.','markersize', 9);
arrayfun (@(i)set(h(i),"color",c(i,:)), 1:N);
hold on
hp = plot (t,yp);
arrayfun (@(i)set(hp(i),"color",c(i,:)), 1:N);
hold off
axis tight
xlabel ('time')
ylabel ('x')
legend ([h(1) hp(1)], {'Simulation','Emulation'},'Location','Northoutside', ...
'orientation','horizontal')

subplot(2,1,2)
nlp = nls(2)+exp(mdp);
Y   = simulator (t, nlp);
par = ddata.parmap (nlp);
x   = struct ('time', t, ...
             'nlp', nlp, ...
             'lp', par(:,1), ...
             'ic', par(:,2:3), ...
             'idx', 1);
yp  = emulate (emu, x);
h = plot (t,Y,'.r','markersize', 9);
hold on
hp = plot (t,yp,'-k');
hold off
axis tight
xlabel ('time')
ylabel ('x')
legend ([h(1) hp(1)], {'Simulation','Emulation'})
