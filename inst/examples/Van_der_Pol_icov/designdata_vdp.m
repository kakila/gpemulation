## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

#######################
##  Data generation  ##
#######################
nT  = 100;                            # Number of time samples
n   = 2;                              # Number of design datasets

T   = 30;                             # Time to wait before observation
t   = linspace (0, 15, nT).';         # Time samples
mu  = linspace (0.3, 7, n)';        # The oscillator parameters
d   = 2;                              # Dimension of the simulator signals

vdp = @(y,t, mu) [y(2); ...                          # Simulator is
                  mu * (1 - y(1)^2) * y(2) - y(1)];  # Van der Pol oscillator

y0        = [0.5 0];                                   # Initialization conditions
simulator = @(t,p) lsode (@(x,t)vdp(x,t,p), ...
        lsode(@(x,t)vdp(x,t,p), y0, [0 T])(end,:), ... # Initial condition
                    t);

y    = zeros (nT, n, d);              # Matrix to store the design datasets

nz = 10;
z  = zeros (nT*3, nz, d);
iz = 0;
idx = zeros(1,nz);
tz = linspace (0,15,3*nT);
for i=1:n
  fprintf ("Generating design data %d of %d\r", i, n); fflush (stdout);
  y(:,i,:) = simulator (t, mu(i));
  if (mod(i,8) == 0 || i == 1)
    iz += 1;
    idx(iz) = i;
    z(:,iz,:) = simulator (tz,mu(i));
  endif
endfor
fprintf ("\n");
p   = 1;           # Dimension of simulator parameter vector
nlp = mu;          # Simulator parameter matrix
