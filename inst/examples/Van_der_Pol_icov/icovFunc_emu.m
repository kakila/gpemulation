## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} icovFunc_emu (@var{}, @var{})
## Inverse of the covaraince function at inputs
##
## @seealso{}
## @end deftypefn

function k = icovFunc_emu (invert, A, covS, h, x, y=[])
  ## FIXME How to deal with noise on IC?
  ## Sherman-Morrison-Woodbury update?
  n = length(x.nlp);

  t  = x.time;
  nt = length (t);
  dt = t(2) - t(1);

  # Eigenspace
  V  = arrayfun (A{1}, x.lp, "unif", 0);
  # Inverse eigenspace
  Vi = arrayfun (A{3}, x.lp, "unif", 0);
  # Eigenvalues
  D  = arrayfun (A{2}, x.lp, "unif", 0);
  m  = length(D{1});

  idxx = [];
  if ~isempty (x.idx)
    ini = (x.idx(:) - 1) * nt + 1;
    fin = x.idx(:) * nt;
    ii  = cell2mat (arrayfun (@colon, ini, fin, "unif", 0)).'(:);
    for i=1:n
      idxx = [idxx; ii+(i-1)*nt*m];
    endfor
  endif
  C  = eye(m);

  if isempty (y) || strcmp (y, 'diag')

    S   = feval (covS{:}, h.S, x.nlp);
    G   = cellfun(@(x,y,z)greenLTIo1diag (t, t, {x,y,z}), V, D, Vi, "unif", 0);
    Gt  = cellfun(@(x,y,z)greenLTIo1diag (t, t, {x,y,z}, 1), V, D, Vi, "unif", 0);

    if (invert == 1)

      iS  = cholinv (S);
      iS  = kron (inv(C), iS);

      iG  = blkdiag(cellfun(@pinv, G, "unif", 0){:});
      iGt = blkdiag(cellfun(@pinv, Gt, "unif", 0){:});

      iGt = reshape (iGt, [nt*n*m*nt n*m]);
      iGt = iGt * iS;
      iGt = reshape (iGt, [nt*n*m nt*n*m]);

      k  = iGt * iG / dt;
    else
      S = kron (S , C);

      G  = blkdiag(G{:});
      Gt = blkdiag(Gt{:});

      G = reshape (G, [nt*n*m*nt n*m]);
      G = G * S;
      G = reshape (G, [nt*n*m nt*n*m]);
      k = G * Gt * dt;

      #FIXME remove before product?
      if ~isempty (idxx)
        k = k(idxx,idxx);
      endif

    endif

  else
    ny = length(y.nlp);
    r  = y.time;
    nr = length (r);

    if nr > nt
      u = r;
      dt = r(2) - r(1);
    else
      u = t;
    endif
    nu = length (u);

    # Eigenspace
    Vy  = arrayfun (A{1}, y.lp, "unif", 0);
    # Inverse eigenspace
    Viy = arrayfun (A{3}, y.lp, "unif", 0);
    # Eigenvalues
    Dy  = arrayfun (A{2}, y.lp, "unif", 0);
    my  = length(Dy{1});

    idxy = [];
    if ~isempty (y.idx)
      ini = (y.idx(:) - 1) * nr + 1;
      fin = y.idx(:) * nr;
      ii  = cell2mat (arrayfun (@colon, ini, fin, "unif", 0)).'(:);
      for i=1:ny
        idxy = [idxy; ii+(i-1)*nr*my];
      endfor
    endif

    S   = feval (covS{:}, h.S, x.nlp, y.nlp);
    G   = cellfun(@(x,y,z)greenLTIo1diag (t, u, {x,y,z}), V, D, Vi, "unif", 0);
    Gt  = cellfun(@(x,y,z)greenLTIo1diag (u, r, {x,y,z}, 1), Vy, Dy, Viy, "unif", 0);

    S = kron (S , eye (m,my));

    G  = blkdiag(G{:});
    Gt = blkdiag(Gt{:});

    G = reshape (G, [nt*n*m*nu n*m]);
    G = G * S;
    G = reshape (G, [nt*n*m nu*ny*my]);
    k = G * Gt * dt;

    #FIXME remove before product?
    if ~isempty (idxx)
      k = k(idxx,:);
    endif
    if ~isempty (idxy)
      k = k(:,idxy);
    endif

  endif

endfunction
