## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function res = parse_emu_args (varargin)
  ######################
  ## Parse parameters ##
  parser = inputParser ();
  parser.FunctionName = 'parse_emu_args';

  # Covariance and mean of dynamical system
  parser.addParamValue ('covFunc', []);
  parser.addParamValue ('icovFunc', []);
  parser.addParamValue ('meanFunc', []);

  # Regularization parameter
  parser.addParamValue ('regParam', 1e-6);

  # Design data
  parser.addParamValue ('input', []);
  parser.addParamValue ('output', []);

  # Conditioning method
  parser.addParamValue ('method', 'chol');

  # memory usage
  valid_modes = {'terse', 'prolix'};
  check       = @(x) ismember (x, valid_modes);
  parser.addParamValue ('memMode', 'prolix', check);

  parser.parse (varargin{:});
  res = parser.Results;

  ######################
endfunction
