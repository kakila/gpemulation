## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} emulate (@var{ME}, @var{t}, @var{p}, @var{y0}, @var{coord})
##
## Calculate emulator's output for @var{t} and @var{p}.
##
## @end defmethod

function [y ym Kpo dy Kpp] = emulate (ME, x)

  nT = length (x.time);
  n  = size (x.nlp,1);

  M  = size (ME.alpha, 1);
  nE = size (ME.input.nlp, 1);
  N  = size (ME.input.time, 1);

  if ( isfield (x,"idx") )
    if ( ~isempty (x.idx) )
      m = length (x.idx);
    endif
  else
    x.idx = [];
    m = M/nE/N;
  endif

  if n == 1
    y = feval (ME.covFunc{:}, x, ME.input) * ME.alpha;
    if (m > 1)
      y  = reshape (y, nT, m);
    endif

  else
    # Separate data in chucks
    nn     = 1e3;
    ndd    = length (ME.alpha);
    Nm     = nT * m;
    b_size = ndd * Nm * sizeof (1.0) / 1024^2; # size in MB
    mm     = memory();
    while ((mm.physical - mm.total)*0.2 < (nn * b_size))
      nn /= sqrt (2);
    endwhile
    nn = min (n, round (nn));
    if exist ("pararrayfun")
      np = nproc - 1;
      error ('not implemented');
    else
      y  = zeros (nT, n, m);
      xx = struct ('time',x.time, 'nlp', [], 'lp', [], 'ic', [], 'idx', x.idx);
      n_ = floor (n / nn);
      idx = 1:nn;
      wh = waitbar (0, sprintf("Emulation %d of %d (batch size %d)", 0, n, nn));
      for i=1:n_
        t0     = cputime ();
        ii     = idx + nn*(i - 1);
        xx.lp  = x.lp(ii,:);
        xx.nlp = x.nlp(ii,:);
        xx.ic  = x.ic(ii,:);
        Kpo = feval (ME.covFunc{:}, xx, ME.input);
        if (m > 1)
          y(:,ii,:) = reshape (Kpo * ME.alpha, nT, nn, m);
        else
          y(:,ii) = reshape (Kpo * ME.alpha, nT, nn);
        endif
        clear Kpo;
        waitbar (i/n_, wh, ...
                 sprintf("Emulation %d of %d (%.1f s)", nn*i, n, cputime()-t0));
      endfor
      close (wh);
      # remainig data
      nn = rem (n, nn);
      if (nn > 0)
        ii     = (ii(end)+1):n;
        xx.lp  = x.lp(ii,:);
        xx.nlp = x.nlp(ii,:);
        xx.ic  = x.ic(ii,:);
        Kpo = feval (ME.covFunc{:}, xx, ME.input);
        if (m > 1)
          y(:,ii,:) = reshape (Kpo * ME.alpha, nT, nn, m);
        else
          y(:,ii) = reshape (Kpo * ME.alpha, nT, nn);
        endif
      endif
#    Kpo = zeros (nT*n*m, size(tmp,2));
#    idx = 1:nT*m;
#    Kpo(idx,:) = tmp;
#    for i=2:n
#      xx.lp  = x.lp(i,:);
#      xx.nlp = x.nlp(i,:);
#      xx.ic  = x.ic(i,:);
#      Kpo(idx + nT*m*(i-1),:) = feval (ME.covFunc{:}, xx, ME.ddata);
#    endfor
    endif
  endif # parallel features

  ym = 0;
  if ~isempty (ME.meanFunc)
    ym = feval (ME.meanFunc{:}, x);
  endif

  if (nargout == 1)
    y += ym;
  elseif (nargout > 3)
    Kpp = feval (ME.covFunc{:}, x, 'diag');
    U   = diag (Kpo * ME.iKoo * Kpo.');
    dy  = Kpp - U;
    if (m > 1)
      dy = reshape (dy, nT, m, n);
      dy = permute (dy, [1 3 2]);
    else
      dy = reshape (dy, nT, n);
    endif
  endif

endfunction

function y = emulate_single(xx, covf, a, nT, m)
  y = covf (xx) * a;
  if (m > 1)
    y = reshape (y, nT, m);
  endif
endfunction
