## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @deftypeop Constructor {@@gpemulator} {@var{E} =} gpemulator (@var{IN},@var{OUT})
## @deftypeopx Constructor {@@gpemulator} {@var{E} =} gpemulator (@dots{},@var{param},@var{value})
##
## @end deftypeop

function E = gpemulator (varargin)

  # mandatory input fields for input vector
  infields = {'time', 'nlp', 'nl'};

  # Basic structure
  E = struct('input_fields', [], ...
             'covFunc', [], ...
             'icovFunc', [], ...
             'meanFunc', [], ...
             'selectFunc', [], ...
             'Koo', [], ...
             'iKoo', [], ...
             'regParam', [], ...
             'mean', [], ...
             'residual', [], ...
             'input', [], ...
             'output', [], ...
             'method', "", ...
             'memMode', ""
             );

  E.input_fields = infields;

  res = parse_emu_args(varargin{:});

  E.covFunc      = res.covFunc;
  E.icovFunc     = res.icovFunc;
  E.meanFunc     = res.meanFunc;
  E.regParam     = res.regParam;
  E.input        = res.input;
  E.output       = res.output;
  E.method       = res.method;
  E.memMode      = res.memMode;

  clear res
  ######################

  # Fill matrices
  if ~isempty (E.input)
    E = set_input (E);
  endif

  if ~isempty (E.output)
    E = set_output (E);
  endif

  E = class (E, "gpemulator");
endfunction

%!assert(!isempty(gpemulator (rand(3), eye(3))))
%!assert(!isempty(gpemulator (rand(3), eye(3), eye(3))))
%!assert(!isempty(gpemulator ({rand(3),randn(3,1),rand(3)}, eye(3), eye(3))))
%!xtest(gpemulator (@(t)0, eye(3)))
