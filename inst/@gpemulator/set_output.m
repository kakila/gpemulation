## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod{@@gpemulator} set_output (@var{me}, @var{y})
## @defmethodx {@@gpemulator} set_output (@var{me})
## Set output design data.
##
## Updates the residual.
## @end defmethod

function ME = set_output (ME, y = [])

  # No input given then use stored one
  if ( isempty (y) )
    if ( isempty (ME.output) )
      error ("gpemulation:invalid-fun-call", ...
             "No output provided and none available.");
    endif
  else
    ME.output = y;
    if ( strcmpi (ME.memMode, "prolix") )
      ME = update_residual(ME);
    endif
  end

endfunction
