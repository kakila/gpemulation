## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function ME = condition (ME)
  if ( strcmpi (ME.memMode, "prolix") )

    if (size(ME.residual,3) > 1)
      res = permute (ME.residual, [1 3 2])(:);
    else
      res = ME.residual(:);
    endif

    M = size (ME.Koo, 1);
    switch ME.method
      case "chol"
        ME.alpha = ME.iKoo * res;
      case "bslash"
        ME.alpha = (ME.Koo + ME.regParam * eye (M)) \ res;
      otherwise
        error ("gpeulation:invalid-input-arg", ...
               "Unkown inversion method.\n");
    endswitch

  else

    # residuals
    res = ME.output - feval (ME.meanFunc{:}, ME.input);
    if (size(res,3) > 1)
      res = permute (res, [1 3 2])(:);
    else
      res = res(:);
    endif

    # Evaluate input covariance function
    ME.alpha  = feval (ME.covFunc{:}, ME.input);
    # Regularize
    ME.alpha += ME.regParam * eye ( length (ME.alpha) );
    # Invert it and multiply residuals
    switch ME.method
      case "chol"
        ME.alpha  = cholinv (ME.alpha) * res;
      case "bslash"
        ME.alpha  = ME.alpha \ res;
      otherwise
        error ("gpeulation:invalid-input-arg", ...
               "Unkown inversion method.\n");
    endswitch

  endif

endfunction
