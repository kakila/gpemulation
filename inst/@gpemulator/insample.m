## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} insample (@var{ME})
##
## Retunrs the insample prediction of the emulator
##
## @end defmethod

function [y x] = insample (ME)
  x = ME.ddata;

  if ~isempty (x.idx)
    m = length (x.idx);
  else
    m = ME.M/ME.n/ME.N;
  endif

  y = ME.Koo * ME.alpha;
  if (m > 1)
    y  = permute(reshape (y, ME.N, m, ME.n),[1 3 2]);
  else
    y  = reshape (y, ME.N, ME.n);
  endif

  if ~isempty (ME.mean)
    y += ME.mean;
  endif

endfunction
