## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} set_input (@var{me}, @var{y})
## @defmethodx {@@gpemulator} set_input (@var{me})
## Set input design data.
##
## Updates the mean and the input covariance matrix.
## @end defmethod

function ME = set_input (ME, x = [])

  # No input given then use stored one
  if ( isempty (x) )
    if ( isempty (ME.input) )
      error ("gpemulation:invalid-fun-call", ...
             "No input provided and none available.");
    endif
  else
    ME.input = x;
  end

  is_prolix = strcmpi (ME.memMode, "prolix");
  if ( is_prolix )
    # Evaluate the mean function
    if !isempty (ME.meanFunc)
      ME.mean = feval (ME.meanFunc{:}, ME.input);
      ME      = update_residual (ME);
    endif
  endif

  switch ME.method
    case {"chol"}

      if ( is_prolix )
        # Evaluate input covariance function
        Koo = feval (ME.covFunc{:}, ME.input);

        #FIXME compute only one side of covariance
        us = max (abs ((Koo - Koo.')(:)));
        if ( us > eps)
          warning ("gpemulation:asymmetric-kernel", ...
                   "Unsymmetric matrix (%g). Symmetrizing.\n", us);
          Koo = (Koo + Koo.') / 2;
        end
        ME.Koo = Koo;
      end

    case {"fitc"}
      # Subsets of data
      if ( ~isempty (ME.selectFunc) )
         idx_n = ME.selectFunc (x);
      elseif ( isfield (x,"include") )
         idx_n = x.include;
      else
          m      = min (max (round (0.05 * ME.n), 10), ME.n);
          idx_n  = randperm (ME.n,m);
      endif
      m           = ME.M/ME.n/ME.N;
      idx         = (1:(ME.N*m)).';
      idx_include = cell2mat (
            arrayfun(@(i)(i-1)*ME.N*m + idx, idx_n(:), ...
            "unif",0)
            );
      ## Separate inputs
      ## Evaluate Knn, Kmn
    otherwise
      error ("gpemulation:invalid-fun-call", ...
             "Unkown inversion method.\n");
  endswitch

endfunction
