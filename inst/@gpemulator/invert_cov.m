## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function ME = invert_cov (ME, varargin)

  if ( strcmpi (ME.memMode, "terse") )
    warning ("gpemulation:nothing-done", ...
       "In terse mode the invere is not stored. Nothing done!\n");
    return
  endif

  res         = parse_emu_args(varargin{:});
  ME.regParam = res.regParam;

  M = size (ME.Koo, 1);

  switch ME.method
    case "chol"

      if ( ME.regParam < 1e-3 )
        ME.iKoo = cholinv (ME.Koo + ME.regParam * eye (M));
      else
        ME.iKoo = cholinv (ME.Koo / ME.regParam + eye (M)) / ...
                  ME.regParam;
      endif

    case "fitc"

      # Select subset of data
      if !isempty (ME.selectFunc)
        idx_n = ME.selectFunc (x);
      elseif isfield (x,"include")
        idx_n = x.include;
      else
        m      = min (max (round (0.05 * ME.n), 10), ME.n);
        idx_n  = randperm (ME.n,m);
      endif
      m          = ME.M/ME.n/ME.N;
      idx         = (1:(ME.N*m)).';
      idx_include = cell2mat (
        arrayfun(@(i)(i-1)*ME.N*m + idx, idx_n(:), ...
        "unif",0));

      Kmm = Koo (idx_include, idx_include);
      Kmn = Koo (idx_include, :);
      iK  = cholinv (Kmn * Kmn.' + ME.regParam * Kmm + ME.regParam * eye(length(idx_include))) * Kmn;
      ME.iKoo  = iK;
      ME.alpha = iK * res;

      ME.ddata.nlp  = ME.ddata.nlp(idx_n,:);
      ME.ddata.lp   = ME.ddata.lp(idx_n,:);
      ME.ddata.ic   = ME.ddata.ic(idx_n,:);
      ME.Koo        = Kmn.';

    otherwise
      error ("Unkown inversion method.\n");
  endswitch

endfunction
