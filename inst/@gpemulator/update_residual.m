## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} update_residual (@var{me})
## Updates residual.
## @end defmethod

function ME = update_residual (ME)

  if (~isempty (ME.output) && ~isempty (ME.mean))
    if ( any (size (ME.output) == size (ME.mean) ) )
      ME.residual = ME.output - ME.mean;
    else
      error ("gpemulation:inconsistency-data", ...
             "Output and mean functions' size do not agree.");
    endif
  endif

endfunction
