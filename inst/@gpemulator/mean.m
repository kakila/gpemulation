## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} mean (@var{ME}, @var{t}, @var{p}=[], @var{y0}=[])
##
## Mean of the emulator.
##
## @end defmethod

function m = mean (ME, x = [])
  m = 0;
  # No input and mean defined
  if (isempty (x) && ~isempty (ME.mean))
    m = ME.mean;
    return
  endif

  # Input given and mean funtion exist
  if (~isempty (x) && ~isempty (ME.meanFunc))
    m = feval (ME.meanFunc{:}, x);
    return
  endif

  # No input and mean function exist
  if (~isempty (x) && ~isempty (ME.meanFunc))
    m = feval (ME.meanFunc{:}, ME.input);
    return
  endif

endfunction
