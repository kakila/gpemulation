## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} set_mean (@var{ME}, @var{func})
## Set the mean function.
## Evaluates if there is input.
## Updates the residual.
## @end defmethod

function ME = set_mean (ME, m)

  # We want to re-define the mean function
  if (iscell (m) && is_function_handle (m{1}))

    ME.meanFunc = m;
    if (~isempty (ME.input))
      ME = feval (ME.meanFunc{:}, ME.input);
    endif

  else

    error ("gpemulation:invalid-fun-call", ...
           "Invalid mean function.");

  endif

  ME = update_residual (ME);

endfunction
