## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} cov (@var{ME}, @var{t}, @var{r}, @var{pt}, @var{pr}=[], @var{coord}=[])
##
## Covaraince of the emulator.
##
## @end defmethod

function K = cov (ME, x, y, coord=[])
  t   = x.time;
  pt  = x.sys;
  x0  = x.ic;
  in1 = x.input;

  r   = y.time;
  pr  = y.sys;
  y0  = y.ic;
  in2 = y.input;

  # LTI
  if (ME.lti)
    # diagonalizable
    if (ME.diag)
      # Same prior for all datasets
      if (numel(ME.priorFunc.V)==1 || is_function_handle (ME.priorFunc.V))
        # pt is an array of 1 rows and Pdim paramters
        Pdim = size (pt,2);
        if !isempty (pr)
          pc = {pt,pr};
        else
          pc = {pt};
        end
        N = numel (pc);

        # Eigenspace
        V    = cellfun (ME.priorFunc.V, pc, "unif", 0); # {1 x N}-(m x m)
        m    = length(V{1});                            # dimension of each replica
        V(1) = sparse (V{1});                           # make sure block diagonla is sparse
        V    = blkdiag (V{:});                          # N*m x N*m

        # Inverse eigenspace
        Vi    = cellfun (ME.priorFunc.V_inv, pc, "unif", 0); # {1 x N}-(m x m)
        Vi(1) = sparse (Vi{1});                              # make sure block diagonal is sparse
        Vi    = blkdiag (Vi{:});                             # N*m x N*m

        # Eigenvalues
        D = cell2mat (cellfun (ME.priorFunc.D, pc, "unif", 0)); # N x m

        # Coupling noise covaraince
        S = feval (ME.noiseCovFunc{:}, x, y);
        # It is N*m x N*m

        # Coupling noise covaraince IC
        S0 = feval (ME.noiseCovFuncIC{:}, x, y);
        # It is N*m x N*m

        # Observed coordinates
        if !isempty (coord)
          coord = cell2mat (arrayfun (@(i)(i-1)*m + coord, 1:N, "unif", 0));
        endif

        K = ME.covFunc (t, r, {V, D, Vi}, S, S0, coord);

      endif
    endif

  endif
endfunction
