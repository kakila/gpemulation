## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defmethod {@@gpemulator} designdata (@var{ME}, @var{x}, @var{y})
##
## Condition the emulator on the design data @var{y}.
##
## @end defmethod

function ME = designdata (ME, x, y)

  printf ("Storing input/output data...\n");
  fflush (stdout);
  ME = set_input (ME, x);
  ME = set_output (ME, y);

  printf ("Conditioning...\n");
  fflush (stdout);
  warning ("off","gpemulation:nothing-done", "local");
  ME = invert_cov (ME);
  ME = condition (ME);

#  [N, n, Odim] = size (y);
#  ME.N         = N;             # # of time samples
#  ME.n         = n;             # # of design datasets

#  ME.ddata = x;

#  Koo = feval (ME.covFunc{:},x);
#  #FIXME compute only one side of covariance
#  us = max (abs ((Koo - Koo.')(:)));
#  if ( us > eps)
#    warning ("Unsymmetric matrix (%g). Symmetrizing.\n", us);
#    fflush (stdout);
#    Koo = (Koo + Koo.') / 2;
#  endif
#  ME.Koo = Koo;
#  ME.M   = size (Koo, 1);  # Total size of conditioned matrix

#  if (strcmpi (method, "icov") || (isempty (method) && ~isempty (ME.icovFunc)))
#    idx = x.idx;
#    x.idx = [];
#    ym = 0;
#    if !isempty (ME.meanFunc)
#      ym      = feval (ME.meanFunc{:}, x);
#      ME.mean = ym;
#    endif
#    ME.residual = y;
#    ME.residual(:,:,idx) -= ym;
#    if (size (y,3) > 1)
#      res = permute(ME.residual, [1 3 2])(:);
#    else
#      res = ME.residual(:);
#    endif

#    ME.iKoo  = feval (ME.icovFunc{:}, x);

#    ini = (idx(:) - 1) * N + 1;
#    fin = idx(:) * N;
#    idx = [];
#    ii  = cell2mat (arrayfun (@colon, ini, fin, "unif", 0)).'(:);
#    m = ME.M/ME.n/ME.N;
#    for i=1:n
#      idx = [idx; ii+(i-1)*N*m];
#    endfor
#    ME.alpha = (ME.iKoo * res)(idx);

#  else
#    ym = 0;
#    if !isempty (ME.meanFunc)
#      ym      = feval (ME.meanFunc{:}, x);
#      ME.mean = ym;
#    endif
#    ME.residual = y - ym;
#    if (size(y,3) > 1)
#      res = permute (ME.residual, [1 3 2])(:)
#    else
#      res = ME.residual(:);
#    endif

#    switch method
#      case {"chol",""}
#        if ME.regParam < 1e-3
#          iK = cholinv (Koo + ME.regParam * eye (ME.M));
#        else
#          iK = cholinv (Koo / ME.regParam + eye (ME.M)) / ME.regParam;
#        end
#        ME.iKoo  = iK;
#        ME.alpha = iK * res;

#      case "bslash"
#        K        = Koo + ME.regParam * eye (ME.M);
#        ME.alpha = K \ res;

#      case "fitc"

#        if !isempty (ME.selectFunc)
#          idx_n = ME.selectFunc (x);
#        elseif isfield (x,"include")
#          idx_n = x.include;
#        else
#          m = min (max (round (0.05 * ME.n), 10), ME.n);
#          idx_n  = randperm (ME.n,m);
#        endif
#        m = ME.M/ME.n/ME.N;
#        idx = (1:(ME.N*m)).';
#        idx_include = cell2mat (arrayfun(@(i)(i-1)*ME.N*m + idx, idx_n(:),"unif",0));
#%        ME.M = ME.N*length(idx_n)*m;
#        % FIXME do not evaluate Koo
#        Kmm = Koo(idx_include, idx_include);
#        Kmn = Koo(idx_include, :);
#        clear Koo
#        iK  = cholinv (Kmn * Kmn.' + ME.regParam * Kmm + ME.regParam * eye(length(idx_include))) * Kmn;
#        ME.iKoo  = iK;
#        ME.alpha = iK * res;

#        %[idx_N idx_m idx_n] = ind2sub ([ME.N m ME.n], idx_include);
#        %ME.ddata.time = ME.ddata.time(idx_N);
#        ME.ddata.nlp  = ME.ddata.nlp(idx_n,:);
#        ME.ddata.lp   = ME.ddata.lp(idx_n,:);
#        ME.ddata.ic   = ME.ddata.ic(idx_n,:);
#        ME.Koo        = Kmn.';
##        ME.ddata.idx_N = idx_N;
##        ME.ddata.idx_n = idx_n;
##        ME.ddata.idx_m = idx_m;

#      otherwise

#        error ("Unkown inversion method.\n");
#    endswitch
#  endif

endfunction
