## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @defun {@var{K} =} meanLTIo1diag (@var{x}, @var{a}, @var{y0})
## @defunx {@var{K} =} meanLTIo1diag (@dots{}, @var{i})
##
## Return the mean function of the multidimensional first order LTI system
##
## @tex
## \dot{y}(t) = A y(t) + \xi(t)
## @end tex
## @ifnottex
## @group
## @verbatim
##  dy
## ---- = A y(t) + xi(t)
##  dt
## @end verbatim
## @end group
## @end ifnottex
##
## with noise with covariance @var{s}, evaluated at the points
## @var{x}.
##
## @end defun

function M = meanLTIo1diag (t, A, y0, u=[], iobs = [], integrator = [])

  t  = t(:);    # first argument is rows
  V  = A{1};
  D  = A{2};
  m  = length (D);
  Vi = A{3};

  # If no index are provided, use the whole space
  if isempty (iobs)
    iobs(1,:) = 1:m;
  endif

  if isempty(integrator)
    nT = length (t);
    D  = D.';

    y_      = zeros (m,nT);
    y_(:,1) = y0;
    dt      = diff (t);
    t_ = t;
    if (std(dt) < eps)
      dt = dt(1);
      if (dt > ((t(end)-t(1)) / 100))
        t  = linspace (t_(1),t_(end),100);
        nT = 100;
        dt = t(2) - t(1);
      endif
    endif

    #FIXME bug in bsxfun and broadcasting prevents cheap memory usage
    if !iscomplex(V)
      eA = arrayfun(@(i)bsxfun(@times, V, exp(D*dt(i))) * Vi, 1:length(dt), "unif", 0);
    else
      V  = full (V);
      eA = arrayfun(@(i)(V .* exp(D*dt(i))) * Vi, 1:length(dt), "unif", 0);
    endif

    k = 1;
    for i = 1:nT-1
      if (length(dt) > 1)
        k = i;
      endif
      y_(:,i+1) = eA{k} * y_(:,i);

      if !isempty (u)
        y_(:,i+1) += u(t(i+1)).';
      endif
    endfor
    M = interp1 (t, y_(iobs,:).',t_);

  else
    A = (V .* D.') * Vi;
    keyboard
    M = integrator (A, t, y0, u)(:,iobs);
  endif

endfunction
