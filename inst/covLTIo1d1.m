## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @defun {@var{K} =} covLTIo1d1 (@var{t}, @var{r}, @var{a}, @var{s}, @var{s0})
## @defunx {@var{dK} =} covLTIo1d1 (@dots{}, @var{i})
## @defunx {[@var{K} @var{K0}] =} covLTIo1d1 (@dots{})
##
## Return the covariance matrix @var{K} of the noise driven first order LTI system
##
## @tex
## \dot{f}(t) = a f(t) + \xi(t)
## @end tex
## @ifnottex
## @group
## @verbatim
##  df
## ---- = a f(t) + xi(t)
##  dt
## @end verbatim
## @end group
## @end ifnottex
##
## with noise with variance @var{s}, evaluated at the points
## @code{@var{t}, @var{r} > 0}. Initial conditions are distributed with covaraince @var{s0}.
##
## If @var{t} is an array with N elements and r is an array with M elements then
## the outputs are N-by-M arrays.
##
## This function is equivalent to @command{covLTIo1diag(@var{x},@var{y},@var{a},@var{s}, @var{s0})}.
## with @code{@var{x} = @{t, a@} } and @code{@var{y} = @{r, a@} }.
##
## The contribution of the noise of the initial condtions is returned in @var{K0}
## if a second output argument is requested.
## In this case the total covaraince matrix is @code{@var{K} + @var{K0}}.
##
## If @code{@var{y} == 'diag'} only the diagonal of the covariance function is evaluated
## at points @var{x}.
##
## If the extra argument @var{i} is not empty, the derivatives of the covariance are returned:
## @table @code
## @item @var{i} == 1
## Derivative w.r.t. @var{a}
## @item @var{i} == 2
## Derivative w.r.t. @var{s}
## @item @var{i} == 3
## Derivative w.r.t. @var{s0}
## @end table
##
## Example:
## @group
## @verbatim
##     t = linspace (0,1,100).';
##     r = 0.5;
##     s = s0 = 1;
##     a = -3;
##     K = covLTIo1d1 (t, r, a, s, s0);
## @end verbatim
## @end group
##
## @seealso{covLTIo1diag}
## @end defun

function [K K0] = covLTIo1d1 (t, r, a, s, s0, i = [])

  t  = t(:);    # first argument is rows
  nt = length (t);
  at = a * t;
  K = K0 = 0;

  if strcmp (r ,'diag')
    at = 2 * at;

    if (isempty (i) || i == 2)

      K = (exp (at) - 1) / a / 2;
      if (isempty (i))
        K *= s;
        K0 = exp (at) * s0;
      end
    elseif (i == 3)

      K0 = exp (at);

    elseif (i == 1)
      K  = ( exp (at) .* (at - 1) + 1 ) * s / a^2 / 2;
      K0 = 2 * a * exp (at) * s0;
    end

    return
  else

    r  = r(:).';  # second argument is columns
    nr = length (r);
    ar = a * r;

    idx = t > r;

    K  = zeros (nt, nr);

    if (isempty (i) || i == 2)

      K(idx)  = ( sinh (ar) .* exp (at) )(idx);  % t > r
      K(!idx) = ( sinh (at) .* exp (ar) )(!idx); % t <= r

      if (i == 2)
        K /= a;
      else
        K *= s / a;
        K0 = exp (at) .* exp(ar) * s0;
      end

    elseif (i == 3)

      K0 = exp (at) .* exp(ar);

    elseif (i == 1)

      K(idx)  = ( (ar .* cosh (ar) + (at - 1) .* sinh(ar) ) .* exp (at) )(idx);  % t > r
      K(!idx) = ( (at .* cosh (at) + (ar - 1) .* sinh(at) ) .* exp (ar) )(!idx); % t <= r

      K *= s / a^2;

      K0 = 2 * a * exp (at) .* exp(ar) * s0;

    end

  end

  if (nargout == 1)
    K = K + K0;
  end
endfunction

%!shared t, r, nt, nr, a, s, s0
%! nt = 10;
%! t  = linspace(0,1,nt);
%! nr = 5;
%! r  = linspace(0,1,nr);
%! a  = -1;
%! s  = 1;
%! s0 = 1;

%!test
%! K = covLTIo1d1 (t, 'diag', a, s, s0);
%! assert (size(K), [nt, 1])

%!test
%! K = covLTIo1d1 (t, t, a, s, s0);
%! assert (size(K), [nt, nt])
%! K = covLTIo1d1 (t, r, a, s, s0);
%! assert (size(K), [nt, nr])

%!test
%! K = covLTIo1d1 (t,'diag', a, s, s0, 1);
%! assert (size(K), [nt, 1])
%! K = covLTIo1d1 (t,'diag', a, s, s0, 2);
%! assert (size(K), [nt, 1])

%!test
%! K = covLTIo1d1 (t, r, a, s, s0, 1);
%! assert (size(K), [nt, nr])
%! K = covLTIo1d1 (t, r, a, s, s0, 2);
%! assert (size(K), [nt, nr])

%!test
%! K = covLTIo1d1 (t, t, a, s, s0, 1);
%! assert (size(K), [nt, nt])
%! K = covLTIo1d1 (t, t, a, s, s0, 2);
%! assert (size(K), [nt, nt])

%!test
%! Kt = covLTIo1d1 (t, t, a, s, s0);
%! [K K0] = covLTIo1d1 (t, t, a, s, s0);
%! assert (all(Kt == K + K0))

%!demo
%! t = linspace (0,1,100).';
%! r = 0.5;
%! s = s0 = 1;
%! a = -3;
%! [K K0] = covLTIo1d1 (t, r, a, s, s0);
%!
%! h = plot (t, K, '-b;Precise IC;', t, K+K0, '-r;Noisy IC;');
%! set (h, 'linewidth', 2);
%! xlabel ('Time')
%! ylabel ('Covariance')
%! axis tight
%! #################################################
%! # Comparing covariance functions with and without.
%! # noisy initial conditons.
%! # The covariance matrix when the IC are not noisy
%! # is singular, because the row(column) corresponding to
%! # the zero value of t(r) will be zero.

%!demo
%! # SDE
%! nT = 1e3;                         # Time samples for Ito integration
%! t_ = linspace (0, 1, nT);         # Time vector for Ito integral
%! dt = t_(2) - t_(1);               # Time step
%! a = -3;
%! eA = exp(a*dt); # Evolution matrix
%! # Ito integral
%! n       = 1e3;
%! dW      = dt * randn (nT, n);   # Wiener differential
%! y_      = zeros (nT,n);
%! y_(1,:) = 1;
%! for i = 1:nT-1
%!  y_(i+1,:) = y_(i,:) * eA + dW(i+1,:);
%! endfor
%! # Experimental covariance
%! ym =  mean (y_, 2);
%! dy = y_ - ym;
%! k  = (dy * dy.')(:,nT/2) / n;
%!
%! # Theoretical covariance
%! t = linspace (t_(1),t_(end), 1e2).';
%! K = covLTIo1d1 (t,0.5,a,1,0);
%!
%! h = plot (t_, k,'-r;Experimental;', t, K,'-k;Exact;');
%! set (h,'linewidth', 2);
%! xlabel ('Time');
%! ylabel ('Covariance [a.u.]')
%! axis tight
%! #################################################
%! # Comparing covariance calculated from n samples and theoretical calculation.

