## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this prograM. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [M n_est] = calc_mem (n, m, N, nData, mData, NData)
  # Reliably estimate usable memory
  mm         = memory ();
  M.physical = mm.physical;
  M.total    = mm.total;
  M.usable   = 0.8 * M.physical - M.total;

  Ksz       = n * m * N;
  Dsz       = nData * mData * NData;
  dbl_MB    = size (1.0) / 1024^2;

  memVec  = Ksz * dbl_MB;   # needed by vectors
  memMat  = Ksz * memVec;   # needed by matrices

  # Final memory usage by gpemulator
  # FIXTHEM: the peak is higher

  # input, output, mean, residual & alpha
  M.prolix.base = memVec * (1+1+1+1+1) + ...
             memMat * (1+1);               # Koo & iKoo
  M.terse.base  = memVec;                  # alpha

  # Memory used by the emulation
  memVecd  = Dsz * dbl_MB;
  memMatd  = Ksz * memVecd;

  M.prolix.data  =  memVecd * (1+1) + memMatd; # res, mean & Kpo
  M.terse.data   =  memVecd;                   # res+mean

  # Memory that can be allocated after conditioning
  # assuming the emulator didn't existed by the time we called
  # memory
  M.prolix.allocatable = M.usable - M.prolix.base;
  M.terse.allocatable  = M.usable - M.terse.base;

  # Number of elems (elems = chuncks*nelem+rest) that can be allocated
  f_elems = floor (M.prolix.allocatable / dbl_MB);
  n_elems = floor (M.prolix.data / dbl_MB);
  if (M.prolix.data <= M.prolix.allocatable)
    n_est.prolix.chunks = 1;
    n_est.prolix.nelm   = f_elems;
    n_est.prolix.rest   = 0;
  else
    n_est.prolix.chunks = floor (n_elems / f_elems);
    n_est.prolix.nelm   = f_elems;
    n_est.prolix.rest   = rem (n_elems, f_elems);
  endif
endfunction
