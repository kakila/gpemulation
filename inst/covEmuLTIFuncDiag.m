## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{} =} covEmuLTIFuncDiag (@var{}, @var{})
##
## @seealso{}
## @end defun

function [K extra] = covEmuLTIFuncDiag (Vf, Df, Vif, Sf, S0f, x, y=[], i=[])

  extra = [];

  # input required fields
  t     = x.time;
  gamma = x.nlparam;
  theta = x.lparam;

  # Number of parameter sets and dimension of phase space
  [n m] = size (theta);

  # Build system matrices for all input parameters
  thetac = mat2cell (theta, ones(n,1), m); # break into cells for performance

  # Eigenspace
  V    = cellfun (Vf, thetac, "unif", 0); # {1 x n}-(m x m)
  V(1) = sparse (V{1});                     # make sure block diagonal is sparse
  V    = blkdiag (V{:});                    # n*m x n*m

  # Inverse eigenspace
  Vi    = cellfun (Vif, thetac, "unif", 0); # {1 x n}-(m x m)
  Vi(1) = sparse (Vi{1});                    # make sure block diagonal is sparse
  Vi    = blkdiag (Vi{:});                   # n*m x n*m

  # Eigenvalues
  D = cell2mat (cellfun (Df, thetac, "unif", 0))(:); # n*m x 1

  n_ = 0;
  if (~isempty(y))
    t_     = y.time;
    gamma_ = y.nlparam;
    theta_ = y.lparam;
    [n_ m]  = size (theta_);

    thetac_ = mat2cell (theta_, ones(n_,1), m); # break into cells for performance
    # Eigenspace
    V_  = cellfun (Vf, thetac_, "unif", 0); # {1 x n_}-(m x m)
    # Inverse eigenspace
    Vi_ = cellfun (Vif, thetac_, "unif", 0); # {1 x n_}-(m x m)
    # Eigenvalues
    D_  = cellfun (Df, thetac_, "unif", 0); # {n_}(m x 1)
  endif

  if (n_ >= 1)
    Y = struct();
    cellfun (@(x)Y.(x)=y.(x)(i,:), fieldnames(y));
    Y.lparam = thetac_{i};

    K = cell (1,n_);
    for i=1:n_
      # Coupling covariance
      S = feval (Sf{:}, x, Y);

      # IC coupling
      S0 = feval (S0{:}, x, Y);

      # Each cell element corresponds to a diagonal element pair
      v   = blkdiag (V, V_{i});
      vi  = blkdiag (Vi, Vi_{i});
      viT = vi.';
      d   = [D; D_{i}];
      G   = greenLTIo1diag (d, t, t_, vi * S * viT, vi * S0 * viT);

      M     = length (d);
      [J I] = meshgrid (1:M, 1:M);
      k     = arrayfun (@(i,j)cell2mat (cmtimes(V(i,:), cmtimes(G,VT(:,j)))), I, J, "unif", 0);
      K{i}  = cell2mat (k);

    endfor

    if (n_ == 1)
      K = K{1};
    endif

  else
      # Coupling covariance
      S = feval (Sf{:}, x);
      # IC coupling
      S0 = feval (S0f{:}, x);

      ViT   = Vi.';
      VT    = V.';
      G     = greenLTIo1diag (D, t, t, Vi * S * ViT, Vi * S0 * ViT);
      M     = length (D);
      [J I] = meshgrid (1:M, 1:M);
      K     = arrayfun (@(i,j)cell2mat (cmtimes(V(i,:), cmtimes(G,VT(:,j)))), I, J, "unif", 0);
      K     = cell2mat (K);

      if (nargout == 2)
        extra = struct ();
        extra.S  = S;
        extra.S0 = S0;
        extra.A  = V * diag(D) * Vi;
      endif

  endif
endfunction
