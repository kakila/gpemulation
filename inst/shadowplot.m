## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} shadowplot (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

function varargout = shadowplot (x,y,uy,ly=[])

  ax = gca();
  if !ishold(ax)
    cla
  endif

  idx = 1:length(x);
  idx1 = [idx idx(end)];
  idx2 = [fliplr(idx) idx(1)];
  X  = x([idx1 idx2]);
  if isempty(ly)
    ly = uy;
  endif
  lY = (y-ly)(idx1);
  uY = (y+uy)(idx2);
  p = patch (X, [lY; uY],[1 0.8 0.8]);
  set (p, 'edgecolor', 'none');

  hold on
  h1 = plot(x, y,'-k');
  h2 = plot(x, y-ly,'color',[1 0.6 0.6]);
  h3 = plot(x, y+uy,'color',[1 0.6 0.6]);
  h = [h1; h2; h3];
  set(h, 'linewidth', 2)

  if !ishold(ax)
    hold off
  endif

  if nargout() > 0
    varargout{1} = struct('line',struct('c',h(1),'b',h(2),'t',h(3)),'patch',p);
  endif

endfunction
