## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @documentencoding UTF-8
## @defun {@var{K} =} covGreenLTIo1diag (@var{x})
## @defunx {@var{K} =} covGreenLTIo1diag (@var{x}, @var{y})
## @defunx {@var{K} =} covGreenLTIo1diag (@dots{}, @var{S})
## @defunx {@var{K} =} covGreenLTIo1diag (@dots{}, @var{S}, @var{S0})
## @defunx {[@var{K} @var{K0}] =} covGreenLTIo1diag (@dots{})
##
## Return the covariance matrix of the noise coupled first order
## LTI system
##
## @tex
## \dot{\boldsymbol{f}}(t) - A \boldsymbol{f}(t) = \boldsymbol{\xi}_g(t) \\
## \dot{\boldsymbol{g}}(t) - B \boldsymbol{g}(t) = \boldsymbol{\xi}_g(t)
## @end tex
## @ifnottex
## @group
## @verbatim
##  df
## ---- = A f(t) + xi_f(t)
##  dt
##
##  dg
## ---- = B f(t) + xi_g(t)
##  dt
##
## @end verbatim
## @end group
## @end ifnottex
##
## with diagonal system matrices A and B.
##
## The input @var{x} is a cell. The first element of these cells indicate the @strong{n} time points
## where the covariance is evaluated, i.e. @code{@var{x}@{1@} = t}. The second
## element of the cell is an array array of @strong{m} eigenvalues A.
##
## The behavior of the function depends on the input arguments in the following way:
##
## @table @asis
##
## @item With a single input argument:
## The function computes and returns the covariance matrix where @var{x}@{1@} is
## an array with @strong{n} input cases. The returned covariance matrix is of
## size @strong{nm}-by-@strong{nm}.
##
## @item With two input arguments:
## If @code{@var{y} == 'diag'} then it returns a vector with the @strong{n} self covariances
## for the test cases in @var{x}.
## If @var{y} is a cell, then it containts @strong{k} test cases in its first element
## and the description of the @strong{l}-by-@strong{l} matrix B in its secod element,
## as described above for the input @var{x}.
## In this case the function returns a @strong{nm}-by-@strong{kl} matrix of cross
## covariances between training cases @var{x} and test cases @var{y}.
##
## @end table
##
## The optional input arguments @var{S} and @var{S0} are the covariance matrix of the
## coupling noise and the initial conditions, respectively. If not provided
## then @code{@var{S} = eye(@strong{m},@strong{k})} (independent systems)
## and @code{@var{S0} = eye(@strong{m},@strong{k})} (independent random initial conditions)
##
## When multiple output argumets are requested the contributions from noisy
## initial conditions is separated. In this case the total covaraince matrix
## is the summ of @var{K} and @var{K0}.
##
## @seealso{covLTIo1diag}
## @end defun
function [G G0] = covGreenLTIo1diag (X, Y = [], S = [], S0 = [])

  t = X{1}; #time
  v = X{2}; #diagonal

  t  = t(:);     # first argument is rows
  v  = v(:);
  nt = length (t);
  nv = length (v);

  flag_diag = false;
  if ~isempty (Y)
    if strcmp (Y, 'diag')
      flag_diag = true;
    else
      r  = Y{1};
      w  = Y{2};
      r  = r(:).';   # second argument is columns
      w  = w(:).';
      nr = length (r);
      nw = length (w);
    endif

  else
    r = t.';
    w = v.';
    nr = nt;
    nw = nv;
  endif

  evt = arrayfun (@(u) exp (u * t), v, 'UniformOutput', false);

  if flag_diag
    nw = nv;
    G0 = G = cell (nv,nv);
    vv = 1./(v + v.');
    for j = 1:nv
      for i = 1:nv
        G{i,j}  = zeros (nt, 1);
        if (abs (v(i) + v(j)) < eps)
          G{i,j}  = exp ( 2 * v(i) *t) .* t;
          G0{i,j} = 1;
        else
          G{i,j}  = ( (evt{j} - 1./evt{i}) .* evt{i} ) * vv(i,j);
          G0{i,j} = evt{i} .* evt{j};
        endif
      endfor
    endfor

  else
    G0 = G = cell (nv,nw);
    vw = v + w;
    idx = t < r;
    evr = arrayfun (@(u) exp (-u * r), v, 'UniformOutput', false);
    ewr = arrayfun (@(u) exp (u * r) , w, 'UniformOutput', false);
    ewt = arrayfun (@(u) exp (-u * t) , w, 'UniformOutput', false);

    for j = 1:nw
      for i = 1:nv
        G{i,j}  = zeros (nt, nr);
        G0{i,j} = evt{i} .* ewr{j};
        if (abs (vw(i,j)) < eps)
          G{i,j}(idx)  = (G0{i,j} .* t)(idx);       % t < r
          G{i,j}(!idx) = (G0{i,j} .* r)(!idx);      % t >= r
        else
          G{i,j}(idx)  = ((evt{i} - ewt{j}) .* ewr{j})(idx) / vw(i,j);    % t < r
          G{i,j}(!idx) = ((ewr{j} - evr{i}) .* evt{i})(!idx) / vw(i,j);   % t >= r
        endif
      endfor
    endfor

  endif

  if ~isempty (S)
    S    = mat2cell (S, ones (nv,1), ones (1,nw));
    G  = cellfun (@times, G, S, "unif", 0);
  endif

  if ~isempty (S0)
    S0   = mat2cell (S0, ones (nv,1), ones (1,nw));
    G0 = cellfun (@times, G0, S0, "unif", 0);
  endif

  if (nargout < 2)
    G = cellfun (@plus, G, G0, "unif", 0);
  endif

endfunction
