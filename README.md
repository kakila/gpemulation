# GP based emulation #
Development files of a GNU Octave package that implements Gaussian Processes based emulation.

## TODO roadmap

* Covariances and likelihoods for 1st and 2nd order linear time invariant systems.
* Interface with gpml
    * Allow for arbitrary noise covariance in the vein of covProd (done)
    * Modify gp to accept vector output
* covGreenLTIo1diag and covLTIo1diag fuctions should return diagonal (done)
* covGreenLTIo1diag and covLTIo1diag fuctions should derivatives
* Allow for full linear prior in gpemulator, that is dx/dt = Ax + Bu; y = Dx + Cu
* Low level implementations of cov functions and Carlo Albert's method for emulation and inversion.
* Optimization of memory usage in function @gpemulator/emulate
* Optimization of processor usage in function @gpemulator/emulate (if parallel is present)
* Optimization of memory usage in function @gpemulator/designdata (via Matrix inverse update)
* Optimization of processor usage in function @gpemulator/designdata (if parallel is present)
* Low level implementations of cov functions and Carlo Albert's method for emulation and inversion.
* Make verbosity optional
