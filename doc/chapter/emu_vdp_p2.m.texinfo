@c This is part of the GNU Octave GP Emulator Package Manual.
@c Copyright 2016-2017 Juan Pablo Carbajal.
@c See the file manual.texinfo for copying conditions.

@documentencoding UTF-8

@example
@group
function A = param2matrix (lp)
  # Egiven-values and -vector functions
  Df  =@@(l) l.*[-J; J];                 # Eigenvalues
  Vf  =@@(l) [ones(1,2); Df(l).'];       # Eigenvectors
  Vif =@@(l) 0.5 * [ones(2,1) 1./Df(l)]; # Inverse of Eigenvectors

  # Eigenspace
  V    = arrayfun (Vf, lp, "unif", 0);
  V(1) = sparse (V@{1@});
  V    = blkdiag (V@{:@});
  # Inverse eigenspace
  Vi    = arrayfun (Vif, lp, "unif", 0);
  Vi(1) = sparse (Vi@{1@});
  Vi    = blkdiag (Vi@{:@});
  # Eigenvalues
  D = cell2mat (arrayfun (Df, lp, "unif", 0));

  A = @{V,D,Vi@};
endfunction

function k = covFunc_emu (covS, covS0, h, x, y=[])

  n = length(x.nlp);
  X = @{x.time, param2matrix(x.lp)@};
  idxx = [];
  if ~isempty (x.idx)
    idxx = cell2mat (arrayfun ( ...
           @@(i)(i-1)*2 + x.idx, 1:n, ...
           "unif", 0));
  endif

  if isempty (y) || strcmp (y, 'diag')

    S  = feval (covS@{:@}, h.S, x.nlp);
    S0 = feval (covS0@{:@}, h.S0, x.ic);
    S  = kron (S, ones(2));
    S0 = kron (S0, ones(2));
    k  = covLTIo1diag (X, y, S ,S0, idxx);

  else
    ny = length(y.nlp);
    Y  = @{y.time, param2matrix(y.lp)@};
    idxy = [];
    if ~isempty (y.idx)
      idxy = cell2mat (arrayfun ( ...
            @@(i)(i-1)*2 + y.idx, 1:ny, ...
            "unif", 0));
    endif

    S  = feval (covS@{:@}, h.S, x.nlp, y.nlp);
    S0 = feval (covS0@{:@}, h.S0, x.ic, y.ic);
    S  = kron (S, ones(2));
    S0 = kron (S0, ones(2));
    k  = covLTIo1diag (X, Y, S ,S0, idxx, idxy);

  endif
endfunction
@end group
@end example
