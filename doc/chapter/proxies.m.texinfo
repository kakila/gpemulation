@c This is part of the GNU Octave GP Emulator Package Manual.
@c Copyright 2016-2017 Juan Pablo Carbajal.
@c See the file manual.texinfo for copying conditions.

@example
@group
#########################
##  Proxy calibration  ##
#########################
proxy = @@(t,p) p(2) * cos (p(1) * t) + p(3)/p(1) * sin (p(1) * t);
q     = 3;                                  # Parameter vector size

z     = zeros (nT, n, 1);                   # Matrix to store the proxies
Q     = zeros (n, q);                       # Matrix of proxy parameters

tmp2 = [1 0 0.5];
for i=1:n
  tmp2   = sqp (tmp2, @@(x)sumsq(y(:,i,1)-proxy(t,x)));
  Q(i,:) = tmp2;
  z(:,i) = proxy (t, tmp2);
endfor

#########################
##  Parameter mapping  ##
#########################
pp     = interp1 (mu, Q, "pp");
parmap = @@(x) ppval(pp, x);
@end group
@end example
