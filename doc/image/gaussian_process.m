## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

N  = 5e2;
nT = 100;
x  = linspace (0,1, nT).';

try
  pkg load gpemulation
  pkg load statistics
  w     = 2*pi*5;
  M     = [0 1; -w^2 -0.1*2*w];
  [V l] = eig(M);
  l     = diag (l);
  K     = covLTIo1diag ({x,{V,l,inv(V)}},[],diag([0 w]),diag([1 0]), 1);
  y     = mvnrnd (0, K, N).';
catch
  warning (lasterr)
  y  = wienrnd (1, N, nT);
  x  = y(:,1); y(:,1) =[];
end

# Slices
slc     = [0.2 0.8];
[~,idx] = min (abs (x-slc));
slc     = x(idx).';

figure (1)
clf
subplot (2,2,1:2)
plot (x,y);
axis tight
v = axis ();

line ([slc; slc], [v(3:4); v(3:4)].', "linewidth", 2);
xlabel ("x")
ylabel ("y")

# slices figure position
p    = get (gca, "position");
pf   = get (gca, "outerposition");
slcf = pf(1) + p(1) + p(3) * (slc - v(1)) / (v(2) - v(1));

for i=1:length(idx)
  s      = std (y(idx(i),:));
  m      = mean (y(idx(i),:));
  [c, b] = hist (y(idx(i),:), 20, 1);
  n      = normpdf (b,m,s); n /= sum (n);

  axes ("parent", gcf, "position", [slcf(i)-p(3)/4 0.1 p(3)/2 0.4])
  %subplot (2,2,2+i)
  bar (b,c,1,"edgecolor","none");
  hold on
  plot (b, n, "-r","linewidth", 2)
  hold off
  axis tight
  set(gca, "visible","off")
endfor
