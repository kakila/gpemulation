## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

try
  source ('parabol_header.mm');
catch
  source ('image/parabol_header.mm');
end

h = plot (X,Y,'.k');
set(h,"markersize", 9);
grid on

axis tight
xlabel ("x");
ylabel ("y");

