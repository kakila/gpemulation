
     # Comparing covariance calculated from n samples of a
     # stochastic differential equation and the theoretical calculation.
     nT = 1e3;                         # Time samples for Ito integration
     t_ = linspace (0, 1, nT);         # Time vector for Ito integral
     dt = t_(2) - t_(1);               # Time step
     a = -3;
     eA = exp(a*dt); # Evolution matrix

     # Ito integral
     n       = 1e3;
     dW      = sqrt(dt) * randn (nT, n);   # Wiener differential
     y_      = zeros (nT,n);
     y_(1,:) = 1;
     for i = 1:nT-1
      y_(i+1,:) = y_(i,:) * eA + dW(i+1,:);
     endfor

     # Experimental covariance
     ym =  mean (y_, 2);
     dy = y_ - ym;
     k  = (dy * dy.')(:,nT/2) / n;

     # Theoretical covariance
     t = linspace (t_(1),t_(end), 1e2).';
     K = covLTIo1d1 (t,0.5,a,dt,0);

     h = plot (t_, k,'-r;Experimental;', t, K,'-k;Exact;');
     set (h,'linewidth', 2);
     xlabel ('Time');
     ylabel ('Covariance')
     axis tight

