## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

try
  source ('parabol_header.mm');
catch
  source ('image/parabol_header.mm');
end

h = plot (X,Y,'.k;data;', ...
          x,ypA,'-r;A;', ...
          x,ypB,'-b;B;', ...
          x,ypB2,'-g;B reg;');
set(h,"linewidth", 2, "markersize", 9);

hold on

hm = plot (x,mxA,'--r;mA;', ...
          x,mxB,'--b;mB;');
set(hm,"linewidth", 1);

hf = plot (x,yfit,':k;fit;');
set(hf,"linewidth", 5);

legend("location","South");

axis tight
xlabel ("x");
ylabel ("y");

hold off

