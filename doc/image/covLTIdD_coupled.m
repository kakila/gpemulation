
     nT = 100;
     t = linspace (0,1,nT).';

     # Oscillator
     w     = 2*pi*5;
     M     = [0 1; -(w)^2 -0.1*2*w];
     [V d] = eig (M);
     Vi    = inv (V);
     d     = diag (d);

     # First order system
     a = -3;

     # combined eigenspace
     V  = blkdiag (1,V);
     Vi = blkdiag (1,Vi);
     d  = [a;d];

     # coupling
     S  = ones(3);                        # Covariance of noise
     S0 = zeros(3);                       # Covariance of inital conditions
     K  = covLTIo1diag ({t,{V,d,Vi}}, [], S, S0);
     K  = covmat2cell (K, [length(t) length(d)]);

     subplot (3,6,[1:3 7:9 13:15])
     colormap (cubehelix);
     imagesc (cell2mat (K));
     set(gca,"visible","off");
     hold on
     line(nT*[0 1; 3 1], nT*[1 0; 1 3]);

     ax = [4 10 16 5 11 17 6 12 18];
     for i=1:9
       subplot (3,6,ax(i));
       [k,l] = ind2sub([3 3],i);
       y = K{i}(:,nT/2);
       plot(t,y,"linewidth", 2);
       set(gca,"visible","off");
       title (sprintf("%d-%d",k,l));
     endfor

