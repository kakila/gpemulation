## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

try
  source ('parabol_header.mm');
catch
  source ('image/parabol_header.mm');
end

subplot (3,1,[1 2])
colormap (cubehelix)
imagesc (x,x,covFunc(x,x))
ylabel ("x");
c = lines(length(x0));
for i=1:length(x0)
  line (x0(i)*[1 1], x([1 end]),'color', c(i,:));
endfor

subplot (3,1,3)

h = plot (x,bx);
for i=1:length(x0)
  set (h(i), "linewidth", 2, "color", c(i,:));
endfor
ylabel ("k(x,x0)")
xlabel ("x");
axis tight

