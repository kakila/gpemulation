## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

clf

plot ([0 1],[-1 1]);
axis tight

pause(1)
