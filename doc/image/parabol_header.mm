## This is part of the GNU Octave GP Emulator Package Manual.
## Copyright 2016-2017 Juan Pablo Carbajal.
## See the file manual.texinfo for copying conditions.

X = linspace (0, 1, 10).';          ## Inputs
x = linspace (0, 1, 100).';         ## Prediction

Yf =@(z) 0.5*(z - 0.5).^2 - 5*(z - 0.5).^4;
Y  = Yf (X);                        ## Observations
y  = Yf (x);                        ## True values

v       = 0.5^2;                           ## Variance of concavity
covFunc =@(x,y) (x(:) - 0.5).^2 .* (y(:).' - 0.5).^2 * v;

meaFuncA =@(x) - 5*(x(:) - 0.5).^4;

KXX = covFunc (X,X); ## Covariance matrix
mX  = meaFuncA (X);   ## Mean vector
x0  = 0.1:0.3:0.7;
bx  = covFunc (x, x0);

resA   = Y - mX;     ## residuals
l      = 1e-8;
alphaA = cholinv ( KXX + l * eye(size(KXX)) ) * resA;

KxX = covFunc (x,X);
mxA  = meaFuncA (x);
ypA = KxX * alphaA + mxA;

meaFuncB =@(x) (x(:) - 0.5).^2;
mX       = meaFuncB (X);   ## Mean vector
resB     = Y - mX;     ## residuals
l        = 1e-8;
alphaB   = cholinv ( KXX + l * eye(size(KXX)) ) * resB;
mxB       = meaFuncB (x);
ypB      = KxX * alphaB + mxB;

l       = 1e0;
alphaB2 = cholinv ( KXX + l * eye(size(KXX)) ) * resB;
ypB2    = KxX * alphaB2 + mxB;

p = (X(:) - 0.5).^2 \ Y;
yfit = p*(x(:)-0.5).^2;

