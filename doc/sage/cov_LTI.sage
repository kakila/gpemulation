
t = var('t',latex_name="t")
r = var('r',latex_name="t'")
dj = var('dj',latex_name="d_{j}")
di = var('di',latex_name="d_{i}")
z = var('z',latex_name="\tau")

f(z) = exp( di * (t - z) ) * exp(-dj * (z - r))
F_tgr = f(z).integrate(z,0,r).canonicalize_radical()
F_tleqr = f(z,r=t,t=r,di=dj,dj=di).integrate(z,0,t).canonicalize_radical()

assume(t>0)
assume(r>0)
assume(di,'complex')
assume(dj,'complex')
Feq_tgr = f(z,dj=-di).integrate(z,0,r).canonicalize_radical()
Feq_tleqr = f(z,r=t,t=r,di=-dj).integrate(z,0,t).canonicalize_radical()

print "\n", "Integral diagonizable first order system A = V D inv(V) for t > r"
print "{}, {} are the elements of the diagonal matrix D".format(latex(di),latex(dj))
print "\n", F_tgr
#print "\n","Latex output:"
#print latex(F_tgr)

print "\n", "For t <= r"
print "\n", F_tleqr
#print "\n","Latex output:"
#print latex(F_tleqr)

print "\n", "When {} = -{}".format(latex(dj),latex(di))
print "\n", Feq_tgr
print "\n", Feq_tleqr

a = var('a', latex_name="\alpha")
print "\n", "Integral 1D first order system A = a"
print "\n", F_tgr(di=a,dj=a)
print "\n", F_tleqr(di=a,dj=a)
