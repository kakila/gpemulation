var('t w c')
# A    = Matrix(SR,2,2,[0,1,-w^2,-c])
A    = Matrix(SR,2,2,[0,1,-w^2,-2*c*w])

D, V = A.eigenmatrix_right()

V = V.expand().simplify_full()

print "Eigenspace"
print V
print "Inverse"
print V.inverse().expand().simplify_full()
print "Eigenvalues"
print D
