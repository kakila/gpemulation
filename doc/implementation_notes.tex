\documentclass[10pt,english,final,a4paper]{article}
\usepackage[usenames,dvipsnames]{color}
\usepackage{hyperref} % Hyperlinks within and outside the document
\hypersetup{
unicode=false,          % non-Latin characters in Acrobat’s bookmarks
pdfauthor={JuanPi Carbajal},%
pdftitle={Implementation notes},%
colorlinks=true,       % false: boxed links; true: colored links
linkcolor=OliveGreen,          % color of internal links
citecolor=Sepia,        % color of links to bibliography
filecolor=magenta,      % color of file links
urlcolor=NavyBlue,          % color of external links
}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern} % German related symbols
\usepackage{fouriernc} % Use the New Century Schoolbook font, comment this line to use the default LaTeX font or replace it with another

\usepackage{hyperxmp}
\usepackage[
type={CC},
modifier={by-sa},
version={4.0},
imagewidth={2cm},
]{doclicense}
\usepackage{minted} % Highlight source code using Pygments
\definecolor{LightGray}{gray}{0.95}
\newcommand{\octcli}[1]{
\mint[bgcolor=LightGray]{octave}|octave> #1|
}
\newcommand{\octv}[1]{
\mintinline{octave}{#1}
}

\usepackage[sort&compress]{natbib}

\usepackage{babel}
\usepackage{graphicx}
\usepackage[small]{caption}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{thmtools}
\usepackage[mathscr]{eucal}

% Customs commmands
% Calculus
\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\defitg}[4]{\int_{#1}^{#2} {#3}\ud {#4}}
\newcommand{\itg}[2]{\int {#1}\ud {#2}}

% Vectors
\newcommand{\bm}[1]{\boldsymbol{#1}}
\newcommand{\bra}[1]{\left\langle  {#1}\right|}
\newcommand{\ket}[1]{\left|{#1}\right\rangle}
\newcommand{\braket}[2]{\left\langle {#1} | {#2} \right\rangle}
% Logicals
\newcommand{\suchthat}{\big \backslash \;}
% Others
\newcommand{\ini}[1]{{}_0#1}
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{#1}\!\times\!{#2}}
\newcommand{\trp}{\top}

% Operators
\DeclareMathOperator*{\err}{err}
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\linspan}{span}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}

% Local for this docuemnt
\newcommand{\szp}{m} % dimnesion of local proxy
\newcommand{\szP}{M} % dimension of aggregated prox
\newcommand{\nT}{n}  % number of time samples
\newcommand{\nD}{N}  % nmber of design datasets (parameter values)
\newcommand{\szK}{\szP\nT}
\newcommand{\Gf}[2]{G\left({#1},{#2}\right)} % Linear dynamical system's Green's function.
\newcommand{\Gfa}[2]{G^{\dagger}\left({#1},{#2}\right)} % Adjoint Green's function.
\newcommand{\Kf}[2]{K\left({#1},{#2}\right)} % Covariance function of emulator.
\newcommand{\Sf}[1]{\Sigma_{#1}} % Covariance function of emulator.
\newcommand{\ppx}{\bm{\theta}} % proxy parameter vector.
\newcommand{\pnl}{\bm{\gamma}} % nl simulator parameter vector.
\newcommand{\LO}[1]{\operatorname{L}_{#1}} % Linear differential operator.
\newcommand{\heavi}[1]{\Theta\left({#1}\right)} % Linear differential operator.
\newcommand{\gaussian}[2]{\mathscr{N}\left({#1},{#2}\right)} % Gaussina distribution

% Comments
\newcommand{\jpi}[1]{{\color{Magenta} JPi: #1}}

\begin{document}
\title{{\Huge GP based emulation package}\\Implementation notes}
\author{Juan Pablo Carbajal}
\date{\today}
\maketitle
\section{List of symbols}

\begin{tabular}{ll}
Symbol & Description \\
\hline
$\szp$ & Dimenson of local proxy's phase space\\
$\nD$  & Number of design datasets (e.g. parameter values) \\
$\szP$ & Dimension of aggregated proxy's phase space $= \sum_i^\nD \szp_i$\\
$\nT$  & Number of time samples in each dataset\\
$\szK$ & Size of the prior GP's covariance matrix\\
$\Gf{t}{t'}$  & Green's function of the linear operator defining the proxy\\
$\Gfa{t}{t'}$  & Adjoint Green's function\\
$\Kf{x}{x'}$  & Emulator's covariance function \\
$\Sf{\bm{\xi}}$    & Covariance of $\bm{\xi}$ \\
$\cov {(\bm{\xi},\bm{\xi}')}$    & Idem \\
$\ppx$ & Proxy's parameter vector \\
$\pnl$ & Model's (or simulator's) parameter vector \\
$\LO{\pi}$ & Linear differential operator with parameters $\pi$ \\
$\heavi{x}$ & Hevisiside step function (half-maximum convention $\heavi{0}=\frac{1}{2}$) \\
$\gaussian{m}{\Sigma}$ & Gaussian distribution with mean $m$ and variance $\Sigma$
\end{tabular}

\section{Gaussian process based emulator}
A Gaussian process based emulator, or GP-emulator, is a GP with inputs in a set of dimension $n$ and outputs on $\mathbb{R}^m$. That is, it is a GP with vectorial outputs.
There is nothing special about these GPs, but they covariance matrices are more complicated to assemble in comparison with scalar GPs, because these functions now need to have information about the covariances across the output components.

A simple vectorial GP is one in which the components of the outputs are not coupled.
This case is already mentioned in~\citep{Rassmuseenbook}.
For these case we obtain a block-diagonal covaraince matrix, where each block
is the covaraince matrix of the corresponding component.

Example: set of independent first order systems (capacitors).

There are many ways to couple the different components.

Carlo's way.
The components are coupled via noise.
That is, compoenents that are coupled, or close ot each other, are driven with highly correlated noise inputs.
Those that are no coupled, or are distant to each other, are driven by independent noise.
The way of measure closeness is given by the distance between the parameters that define each component.

\section{Covariance functions of linear dynamical systems}
\begin{equation}
t,t',\mu,\tau \geq 0
\end{equation}

\begin{equation}
\Kf{t}{t',\ppx} = \defitg{0}{t}{ \Gf{t}{\tau,\ppx}
\defitg{0}{\tau}{  \,\Sf{\xi}\, (\mu,t',\ppx) \Gfa{\tau}{\mu,\ppx} }{\mu}
}{\tau}
\end{equation}

\begin{align}
\Kf{t}{t',\ppx} =& \defitg{0}{t}{ \Gf{t}{\tau,\ppx}
\defitg{0}{\tau}{  \,\Sf{\xi}\, (\ppx)\delta(\mu-t') \Gfa{\tau}{\mu,\ppx} }{\mu}
}{\tau} \stackrel{t > t'}{=} \\
&\defitg{0}{t}{ \Gf{t}{\tau,\ppx}  \,\Sf{\xi}\, (\ppx) \Gfa{\tau}{t',\ppx}}{\tau}
%& \doteq \Kf{t , t'}{\ppx}_{t > t'}
\end{align}

\subsection{First order time invariant system}
\begin{equation}
\LO{\ppx} = \tder{}{t} - A_{\ppx}
\end{equation}

$A_{\ppx} \in \mathbb{R}^{\stimes{s}{s}}$

\begin{equation}
\Gf{t}{t',\ppx} = \heavi{t-t'}e^{A_{\ppx}\, \left(t - t'\right)}
\end{equation}

\begin{equation}
\Gfa{t}{t',\ppx} = \heavi{t'-t}e^{-A^{\trp}_{\ppx}\, \left(t - t'\right)}
\end{equation}

$\xi \sim \gaussian{0}{ \,\Sf{\xi}\, (\ppx)}$

\begin{gather}
\Kf{t , t'}{\ppx} = \defitg{0}{t}{ \heavi{t-\tau}\heavi{t'-\tau} e^{A_{\ppx}\, \left(t - \tau\right)}  \,\Sf{\xi}\, (\ppx)e^{-A^{\trp}_{\ppx}\, \left(\tau - t'\right)}}{\tau} = \\
\defitg{0}{t'}{ e^{A_{\ppx}\, \left(t - \tau\right)}  \,\Sf{\xi}\, (\ppx)e^{-A^{\trp}_{\ppx}\, \left(\tau - t'\right)}}{\tau}
\end{gather}

\subsubsection{Diagonizable matrix}
$A = V D V^{-1}$, %$t_{>} = \max{(t,t')}$, $t_{<} = \min{(t,t')}$

${}_{\xi}\!\Lambda = V^{-1}  \Sf{\xi} \left(V^{-1}\right)^{\trp}$

$D_{ij} = D_i \delta_{ij}$

$\left(e^D\right)_{ij} = \delta_{ij}e^{D_i}$

Drop dependencies on $\theta$ for clarity.

\begin{align}
\Kf{t}{t'} =& \defitg{0}{t'}{ e^{V D V^{-1}\, \left(t - \tau\right)}  \,\Sf{\xi}\, e^{-\left(V^{-1}\right)^{\trp} D V^{\trp}\, \left(\tau - t'\right)}}{\tau} = \\
=&\defitg{0}{t'}{ Ve^{D \left(t - \tau\right)}\, V^{-1}  \Sf{\xi} \left(V^{-1}\right)^{\trp} \, e^{-D \left(\tau - t'\right)}V^{\trp}}{\tau} = \\
=&V\,\left(\defitg{0}{t'}{e^{D \left(t - \tau\right)}\, {}_{\xi}\!\Lambda \, e^{-D \left(\tau - t'\right)}}{\tau}\right)\,V^{\trp}
\end{align}

The integrand

\begin{align}
E_{ij} =& \left(e^{D \left(t - \tau\right)}\, {}_{\xi}\!\Lambda \, e^{-D \left(\tau - t'\right)}\right)_{ij}\\
=& \sum_{k=1}^{s}\left(e^{D \left(t - \tau\right)}\right)_{ik} \, \sum_{l=1}^{s}{}_{\xi}\!\Lambda_{kl} \, \left(e^{-D \left(\tau - t'\right)}\right)_{lj} \\
=& \sum_{k=1}^{s}\delta_{ik} e^{D_i\left(t - \tau\right)} \, \sum_{l=1}^{s}{}_{\xi}\!\Lambda_{kl} \, \delta_{lj} e^{-D_l\left(\tau - t'\right)} \\
=& \sum_{k=1}^{s}\delta_{ik} e^{D_i\left(t - \tau\right)} \, {}_{\xi}\!\Lambda_{kj} \, e^{-D_j \left(\tau - t'\right)} \\
=& {}_{\xi}\!\Lambda_{ij} \, e^{D_i \left(t - \tau\right)} e^{-D_j \left(\tau - t'\right)}
\end{align}

\begin{align}
\left(\defitg{0}{t'}{E}{\tau}\right)_{ij} =& \defitg{0}{t'}{E_{ij}}{\tau} \\
=& {}_{\xi}\!\Lambda_{ij} \, \defitg{0}{t'}{e^{D_i \left(t - \tau\right)} e^{-D_j \left(\tau - t'\right)}}{\tau} \\
%=& {}_{\xi}\!\Lambda_{ij} \, \frac{e^{\left(D_i + D_j\right)t'} - 1}{D_i + D_j} e^{D_i \left(t - t'\right)}\\
=& \frac{{}_{\xi}\!\Lambda_{ij}}{D_i + D_j} \, \left(e^{D_j t'} - e^{-D_i t'}\right) e^{D_i t}\\
\doteq& \left(\Gamma(t,t')\right)_{ij}
\end{align}

Symmetry of $\Kf{t}{t'}$ is inherited from $\Gamma(t,t')$. This implies that for $t \leq t'$ we obtain:
\begin{equation}
\left(\Gamma(t,t')\big\vert_{t < t'}\right)_{ij} = \left(\Gamma(t',t)\right)_{ji} = \frac{{}_{\xi}\!\Lambda_{ij}}{D_i + D_j} \, \left(e^{D_i t} - e^{-D_j t}\right) e^{D_j t'}
\end{equation}

\begin{align}
\left(\Kf{t}{t'}\right)_{ij} %=& \sum_{k=1}^{s} V_{ik} \sum_{l=1}^{s} \left(\Gamma(t,t')\right)_{kl} V^{\trp}_{lj} \\
=& \sum_{k=1}^{s} V_{ik} \sum_{l=1}^{s}
\left.\begin{cases}
\left(\Gamma(t,t')\right)_{kl} & t > t' \\
\left(\Gamma(t',t)\right)_{lk} & t \leq t'
\end{cases} \right\rbrace V^{\trp}_{lj}
\end{align}

\subsubsection{Derivatives w.r.t $\theta$}
\begin{equation}
X_{ij} = \frac{e^{D_j t'} - e^{-D_i t'}}{D_i + D_j} e^{D_i t}
\end{equation}

\begin{align}
\tder{X_{ij}}{\theta_k} =& \\
+& X_{ij} \, t \, \tder{D_i}{\theta_k} +\\
+& e^{D_i t} \left[ t' \frac{\tder{D_j}{\theta_k} e^{D_j t'} + \tder{D_i}{\theta_k} e^{-D_i t'}}{D_i + D_j} + \right. \\
& \phantom{+e^{D_i t}[} - \left. \left( e^{D_j t'} - e^{-D_i t'}\right)\frac{\tder{D_j}{\theta_k} + \tder{D_i}{\theta_k}}{\left(D_i + D_j\right)^2}\right]
\end{align}

\subsubsection{One dimensional system}
$A = \alpha$

\begin{equation}
\Gamma(t,t')  = \frac{\sigma_{\xi}^2}{\alpha} \sinh(\alpha t') e^{\alpha t}
\end{equation}

\begin{equation}
\Kf{t}{t'} = \frac{\sigma_{\xi}^2}{\alpha} \begin{cases}
\sinh(\alpha t') e^{\alpha t} & t > t' \\
\sinh(\alpha t) e^{\alpha t'} & t \leq t'
\end{cases}
\end{equation}

\begin{equation}
\tder{\Kf{t}{t'}}{\alpha} = \frac{\sigma_{\xi}^2}{\alpha^2} \begin{cases}
\left[\alpha t' \cosh(\alpha t') +  \left(\alpha t  - 1\right) \sinh(\alpha t')\right]e^{\alpha t} & t > t' \\
\left[\alpha t  \cosh(\alpha t ) +  \left(\alpha t' - 1\right) \sinh(\alpha t)\right]e^{\alpha t'} & t \leq t'
\end{cases}
\end{equation}

\section{Data structure}
LTI system with system matrix
\begin{equation}
A = \begin{bmatrix}
a_{11} & \ldots & a_{1\szp} \\
       & \ddots &        \\
a_{\szp1} & \ldots & a_{\szp\szp}
\end{bmatrix}
\end{equation}

\begin{equation}
\bm{x}(t) = \begin{bmatrix}
x_{1}(t) \\
\vdots \\
x_{\szp}(t)
\end{bmatrix}
\end{equation}
%\noindent where the elements can be blocks $a_{ij} \in \mathbb{R}^{\stimes{m_i}{m_j}}$ .
The covariance matrix is built
\begin{equation}
K(t,r) = \begin{bmatrix}
K_{11}(t,r) & \ldots & K_{1\szp}(t,r) \\
       & \ddots &        \\
K_{\szp1}(t,r) & \ldots & K_{\szp\szp}(t,r)
\end{bmatrix}
\end{equation}
%\noindent where each block $K_{ij}(t,r) \in \mathbb{R}^{\stimes{m_i}{m_j}}$.

When evaluated at a set of points $t = \bset{t_i}_1^{n_t}$, $r = \bset{r_i}_1^{n_r}$
\begin{equation}
K_{ij}(t,r) = \begin{bmatrix}
\cov\left(x_i(t_1),x_j(r_1)\right) & \ldots & \cov\left(x_i(t_1),x_j(r_{n_r})\right) \\
       & \ddots &        \\
\cov\left(x_i(t_{n_t}),x_j(r_1)\right) & \ldots & \cov\left(x_i(t_{n_t}),x_j(r_{n_r})\right)
\end{bmatrix}
\end{equation}

Hence $K(t.r) \in \mathbb{R}^{\stimes{\szp n_t}{\szp n_r}}$

\begin{equation}
\bm{x}(t) = \begin{bmatrix}
x_{1}(t_1) \\
\vdots \\
x_{1}(t_{n_t})\\
\vdots \\
x_{\szp}(t_1)\\
\vdots \\
x_{\szp}(t_{n_r})
\end{bmatrix}
\end{equation}

A block diagonal composed system
\begin{equation}
\bm{A} = \begin{bmatrix}
A_1 &        &  \\
    & \ddots & \\
    &        & A_{\nD}
\end{bmatrix}
\end{equation}
\noindent $A_i \in \mathbb{R}^{\stimes{\szp_i}{\szp_i}}$ hence $\bm{A} \in \mathbb{R}^{\stimes{\szP}{\szP}}$ with $\szP = \sum_i^\nD \szp_i$.

\begin{equation}
\bm{X}(t) = \begin{bmatrix}
\bm{x}_{1}(t) \\
\vdots \\
\bm{x}_{\nD}(t)
\end{bmatrix}
\end{equation}
\noindent $\bm{X} \in \mathbb{R}^{\szP}$.

\input{implementation_Octave.tex}
\input{implementation_R.tex}
\input{implementation_Python.tex}
\input{implementation_Julia.tex}
\input{implementation_C++.tex}

\end{document}
